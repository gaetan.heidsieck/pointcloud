# GroIMP point cloud plugin

A plugin for additional functions for point clouds for the FSPM platform GroIMP


## Functionalities
- file handling:
    - open .xyz files as a new GroIMP project
    - add .xyz file to existing 
    - export point cloud to .xyz file
- Point cloud modeling:
    - split point cloud
    - merge point cloud
    - duplicate point cloud
- Point cloud processing:
    - clustering point clouds based on the nearest neighbor
    - turn 3D model into a point cloud based on contour points
    - turning a point cloud into a cloud of points
- Fitting
    - fitting point clouds to geometrical objects
    - fitting point cloud to mesh




### File handling:

The plugin handles point clouds defined in .xyz files of the structure:
```
<x>,<y>,<z>
<x>,<y>,<z>
<x>,<y>,<z>
```
For the current state of the project, .xyz files with preset symbols are not supported.

### Modeling

#### Splitting

To split, the point cloud and a plane (defining the Splitting line) must be selected

#### Merging

Merges all selected point clouds into a new point cloud

#### Duplicate
creates a copy of selected point cloud nodes and adds them to the project graph


### Point cloud processing

#### Clustering

Turns one point cloud into several point clouds based on the nearest neighbor. 

#### Turning 3D model to point cloud

Turns the howl 3d model of the project into a point cloud based on the ply export protocol

#### Turning a point cloud into a cloud of points

The point cloud, which is defined by a single node in the project graph, is replaced by individual 3D points.

** Known issue:** the newly added points are referring to the coordinates (0,0,0) rather than the root of the original point cloud 

### Fitting

Selected point clouds can be fitted to different geometrical objects. 
Currently supported objects:
- Sphere
- Cylinder
- Frustum
- Cone

Additionally, it is possible to let the plugin auto-detect the best-fitting shape

#### Mesh nodes

Additionally, it is possible to transform the point cloud into a fully connected mesh node.

** Known issue:** the newly added Meshs refer to the coordinates (0,0,0) rather than the root of the original point cloud.