/**
 * 
 */
/**
 * @author tim
 *
 */
module pointcloud {


	requires graph;
	requires imp;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.core;
	requires xl.impl;
	requires imp3d;
	requires rgg;
	requires java.desktop;
	
}
