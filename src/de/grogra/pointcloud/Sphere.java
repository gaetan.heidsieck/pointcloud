/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a sphere. This class is used for the point cloud
 * fitting feature because it is much more simple to use than the original
 * class.
 *
 * @author Maurice Mueller
 */
public class Sphere extends Shape3D {
	/**
	 * The radius of this sphere
	 */
	private double radius;
	/**
	 * Creates a sphere object with a position, a direction and a radius.
	 *
	 * @param position The position for this sphere
	 * @param direction The direction for this sphere
	 * @param radius The radius for this sphere
	 */
	public Sphere(Vector position, Vector direction, double radius) {
		super(position, direction);
		this.radius = radius;
	}
	/**
	 * Sets the radius of this sphere.
	 *
	 * @param radius The new radius for this sphere
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}
	/**
	 * Returns the radius of this sphere.
	 *
	 * @return The radius of this sphere
	 */
	public double getRadius() {
		return this.radius;
	}
	/**
	 * Calculates and returns the minimum distance of the given point to the
	 * surface of this sphere.
	 *
	 * @param point The point to get the minimum distance to the surface of this
	 * sphere for
	 * @return The minimum distance of the given point to the surface of this
	 * sphere
	 */
	public double getMinimumDistanceToSurface(Point point) {
		Vector position = this.getPosition();
		double x = point.getX() - position.get(0);
		double y = point.getY() - position.get(1);
		double z = point.getZ() - position.get(2);
		double distance = Math.sqrt(x * x + y * y + z * z);
		return Math.abs(distance - this.radius);
	}
	/**
	 * Calculates and returns the volume of this sphere.
	 *
	 * @return The volume of this sphere
	 */
	public double getVolume() {
		return Math.PI * this.radius * this.radius * this.radius * 4.0 / 3.0;
	}
	/**
	 * Calculates and returns the surface of this sphere.
	 *
	 * @return The surface of this sphere
	 */
	public double getSurface() {
		return 4.0 * Math.PI * this.radius * this.radius;
	}
}