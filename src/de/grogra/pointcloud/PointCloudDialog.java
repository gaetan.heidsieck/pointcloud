/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import java.awt.GridLayout;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
/**
 * This class provides methods to show dialog fields for the point cloud menu.
 *
 * @author Maurice Mueller
 */
public class PointCloudDialog {
	/**
	 * The indicator for the file chooser dialog to show the open dialog
	 */
	public static final int OPEN_FILE = 0;
	/**
	 * The indicator for the file chooser dialog to show the save dialog
	 */
	public static final int SAVE_FILE = 1;
	/**
	 * The option for the case that the user wants to cluster a point cloud
	 */
	public static final int CLUSTER = 0;
	/**
	 * The option for the case that the user wants to split a point cloud
	 */
	public static final int SPLIT = 1;
	/**
	 * The option for the case that the user wants to merge point clouds
	 */
	public static final int MERGE = 2;
	/**
	 * The option for the case that the user wants to fit one object to a point
	 * cloud
	 */
	public static final int FIT_ONE = 3;
	/**
	 * The option for the case that the user wants to fit multiple objects to
	 * multiple point clouds
	 */
	public static final int FIT_MULTIPLE = 4;
	/**
	 * The indicator for the case that old point clouds should be kept while the
	 * new ones are added to the graph
	 */
	public static final int KEEP = 0;
	/**
	 * The indicator for the case that old point clouds should be removed while
	 * the new ones are added to the graph
	 */
	public static final int REMOVE = 1;
	/**
	 * The indicator for the case that the user does not want to keep or remove
	 * old point clouds and canceled the interaction
	 */
	public static final int CANCEL = 2;
	/**
	 * The default distance for the point cloud clustering algorithm
	 */
	private static final String DEFAULT_DISTANCE = String.valueOf(Resources.getFloat("defaultPointDistance"));
	/**
	 * The default minimum number of neighbors for the point cloud clustering
	 * algorithm
	 */
	private static final String DEFAULT_MINIMUM_NEIGHBORS = String.valueOf(Resources.getInt("defaultMinimumNeighbors"));
	/**
	 * The default octree depth for the point cloud clustering algorithm
	 */
	private static final String DEFAULT_OCTREE_DEPTH = String.valueOf(Resources.getInt("defaultOctreeDepth"));
	/**
	 * The default remove-noise-cluster indicator for the point cloud clustering
	 * algorithm
	 */
	private static final boolean DEFAULT_REMOVE_NOISE_CLUSTER = Resources.isBoolean("defaultRemoveNoiseCluster");
	/**
	 * The default precision for the point cloud fitting algorithms
	 */
	private static final String DEFAULT_FITTING_PRECISION = String.valueOf(Resources.getInt("defaultFittingPrecision"));
	/**
	 * The default bounding option for the point cloud fitting algorithms. This
	 * value can be set to "MAXIMUM" and "AVERAGE". If "MAXIMUM" is used, all 3d
	 * shapes are fitted so that all points are located inside the shape. If
	 * "AVERAGE" is used, all shapes are fitted so that a linear regression is
	 * executed for each height (length component) of the object. Then, the
	 * values of the linear regression are used for the bottom and top radius.
	 */
	private static final String DEFAULT_FITTING_OPTION = Resources.getString("defaultFittingOption");
	/**
	 * The static instance of this class. It can be requested each time this
	 * class is used with object based methods.
	 */
	private static PointCloudDialog instance;
	/**
	 * The text field that is used to request the distance in the point cloud
	 * clustering algorithm
	 */
	private JTextField distanceField;
	/**
	 * The text field that is used to request the minimum number of neighbors in
	 * the point cloud clustering algorithm
	 */
	private JTextField minimumNeighborsField;
	/**
	 * The text field that is used to request the octree depth in the point
	 * cloud clustering algorithm
	 */
	private JTextField octreeDepthField;
	/**
	 * The check box field that is used to request whether the noise cluster
	 * should be removed in the point cloud clustering algorithm
	 */
	private JCheckBox removeNoiseClusterField;
	/**
	 * The text field that is used to request the precision in the point cloud
	 * fitting algorithm
	 */
	private JTextField precisionField;
	/**
	 * The radio button field that is used to represent the maximum option
	 * during the request of the fitting option in the point cloud clustering
	 * algorithm
	 */
	private JRadioButton fittingOptionMaximumField;
	/**
	 * The radio button field that is used to represent the average option
	 * during the request of the fitting option in the point cloud clustering
	 * algorithm
	 */
	private JRadioButton fittingOptionAverageField;
	/**
	 * The button group that is used during the request of the fitting algorithm
	 * parameters to store the radio buttons (maximum option and average option)
	 */
	private ButtonGroup fittingOptionButtonGroup;
	/**
	 * The directory that was lastly used in the file open or save dialog
	 */
	private File lastDirectory;
	/**
	 * A constructor to initialize the static instance of this class. This
	 * constructor should only be called once by the getInstance() method. It
	 * initializes the input fields and resets the input field values.
	 */
	private PointCloudDialog() {
		this.distanceField = new JTextField();
		this.minimumNeighborsField = new JTextField();
		this.octreeDepthField = new JTextField();
		this.removeNoiseClusterField = new JCheckBox();
		this.precisionField = new JTextField();
		this.fittingOptionMaximumField = new JRadioButton();
		this.fittingOptionAverageField = new JRadioButton();
		this.fittingOptionButtonGroup = new ButtonGroup();
		this.fittingOptionButtonGroup.add(this.fittingOptionMaximumField);
		this.fittingOptionButtonGroup.add(this.fittingOptionAverageField);
		this.lastDirectory = new File(System.getProperty("user.home"));
		this.resetClusteringParameters();
		this.resetFittingParameters();
	}
	/**
	 * Returns the static instance of this class. If the instance is requested
	 * for the first time (and is null), it is initialized.
	 *
	 * @return The instance of this class
	 */
	public static PointCloudDialog getInstance() {
		if (PointCloudDialog.instance == null) {
			PointCloudDialog.instance = new PointCloudDialog();
		}
		return PointCloudDialog.instance;
	}
	/**
	 * Resets the parameters for the cluster parameter input dialog. When this
	 * method is called, the values are reset to the default values, specified
	 * in this class. When this method is not called, the values of the last
	 * time are reused in the next input dialog.
	 */
	private void resetClusteringParameters() {
		this.distanceField.setText(PointCloudDialog.DEFAULT_DISTANCE);
		this.minimumNeighborsField.setText(PointCloudDialog.DEFAULT_MINIMUM_NEIGHBORS);
		this.octreeDepthField.setText(PointCloudDialog.DEFAULT_OCTREE_DEPTH);
		this.removeNoiseClusterField.setSelected(PointCloudDialog.DEFAULT_REMOVE_NOISE_CLUSTER);
	}
	/**
	 * Resets the parameters for the fit parameter input dialog. When this
	 * method is called, the values are reset to the default values, specified
	 * in this class. When this method is not called, the values of the last
	 * time are reused in the next input dialog.
	 */
	private void resetFittingParameters() {
		this.precisionField.setText(PointCloudDialog.DEFAULT_FITTING_PRECISION);
		boolean isAverage = PointCloudDialog.DEFAULT_FITTING_OPTION.equals("AVERAGE");
		this.fittingOptionMaximumField.setSelected(!isAverage);
		this.fittingOptionAverageField.setSelected(isAverage);
	}
	/**
	 * Shows a simple popup message with a title and a message. This box has
	 * only an OK button and can be used when the user should be informed about
	 * errors or other information (graphically).
	 *
	 * @param title The title for the information box
	 * @param message The message for the information box
	 */
	public static void popup(String title, String message) {
		JOptionPane.showMessageDialog(null, message , title, JOptionPane.OK_OPTION);
	}
	/**
	 * Shows a confirm dialog with the given title and the given message. The
	 * dialog has a YES and a NO option. If YES was clicked, true is returned.
	 * Otherwise, false is returned.
	 *
	 * @param title The title for the information box
	 * @param message The message for the information box
	 * @return true If the YES button was clicked and false otherwise
	 */
	public static boolean confirm(String title, String message) {
		int result = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
		return result == JOptionPane.YES_OPTION;
	}
	/**
	 * Asks the user whether an old point cloud or old point clouds should be
	 * kept or removed when new ones are added. The given mode is used to ask
	 * slightly different questions, depending on which objects are kept or
	 * removed and which are created. In all three cases, the user has three
	 * options. Depending on what the user clicked, one of the tree results is
	 * returned. The mode parameter and the return value use the constants in
	 * this class. If an invalid mode is used as parameter, the CANCEL return
	 * value is returned.
	 *
	 * @param mode The CLUSTER, SPLIT or MERGE constant of this class, depending
	 * on what the dialog box should describe
	 * @return The KEEP, REMOVE or CANCEL constant of this class, depending on
	 * what the user clicked
	 */
	public static int keepRemoveCancelPopup(int mode) {
		String title;
		StringBuffer buffer = new StringBuffer();
		if (mode == PointCloudDialog.CLUSTER || mode == PointCloudDialog.SPLIT) {
			title = Resources.translate("keepPointCloud");
			buffer.append(Resources.translate("keepPointCloudQuestion") + "\n\n");
			buffer.append(Resources.translate("keepQuestionKeepCluster") + "\n");
			buffer.append(Resources.translate("keepQuestionRemoveCluster") + "\n");
		} else if (mode == PointCloudDialog.MERGE) {
			title = Resources.translate("keepPointClouds");
			buffer.append(Resources.translate("keepPointCloudsQuestion") + "\n\n");
			buffer.append(Resources.translate("keepQuestionKeepMerge") + "\n");
			buffer.append(Resources.translate("keepQuestionRemoveMerge") + "\n");
		} else if (mode == PointCloudDialog.FIT_ONE) {
			title = Resources.translate("keepPointCloud");
			buffer.append(Resources.translate("keepPointCloudQuestion") + "\n\n");
			buffer.append(Resources.translate("keepQuestionKeepFitOne") + "\n");
			buffer.append(Resources.translate("keepQuestionRemoveFitOne") + "\n");
		} else if (mode == PointCloudDialog.FIT_MULTIPLE) {
			title = Resources.translate("keepPointClouds");
			buffer.append(Resources.translate("keepPointCloudsQuestion") + "\n\n");
			buffer.append(Resources.translate("keepQuestionKeepFitMultiple") + "\n");
			buffer.append(Resources.translate("keepQuestionRemoveFitMultiple") + "\n");
		} else {
			return PointCloudDialog.CANCEL;
		}
		String message = buffer.toString();
		// TODO: Move this to the resources file
		Object[] buttons = new Object[]{"Keep", "Remove", "Cancel"};
		int option = JOptionPane.YES_NO_CANCEL_OPTION;
		int plain = JOptionPane.PLAIN_MESSAGE;
		int result = JOptionPane.showOptionDialog(null, message, title, option, plain, null, buttons, null);
		if (result == JOptionPane.YES_OPTION) {
			return PointCloudDialog.KEEP;
		} else if (result == JOptionPane.NO_OPTION) {
			return PointCloudDialog.REMOVE;
		} else if (result == JOptionPane.CANCEL_OPTION) {
			return PointCloudDialog.CANCEL;
		}
		return PointCloudDialog.CANCEL;
	}
	/**
	 * Returns a full path to a file. The file can be chosen by the user. This
	 * method shows an open-file dialog or a save-file dialog, depending on the
	 * given mode. The mode must be one of the static fields OPEN_FILE and
	 * SAVE_FILE of this class. If the user selected a non-existing file in case
	 * of an open-dialog, null is returned. If the user selected a non-file
	 * object (like a directory or a file link), null is also returned. Only in
	 * case of a valid file the absolute file path is returned.
	 *
	 * @param mode The mode must be one of the fields OPEN_FILE and SAVE_FILE
	 * @return The absolute file path in case of a valid file or null in case of
	 * an invalid file selection
	 */
	public String selectFile(int mode) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(this.lastDirectory);
		int result = Integer.MIN_VALUE;
		if (mode == PointCloudDialog.OPEN_FILE) {
			result = fileChooser.showOpenDialog(null);
		} else if (mode == PointCloudDialog.SAVE_FILE) {
			result = fileChooser.showSaveDialog(null);
		}
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			String message = Resources.translate("fileMustBeRealFileAndExistError");
			if (mode == PointCloudDialog.OPEN_FILE) {
				if ((!selectedFile.exists()) || (!selectedFile.isFile())) {
					String title = Resources.translate("fileOpenError");
					PointCloudDialog.popup(title, message);
					return null;
				}
			} else if (mode == PointCloudDialog.SAVE_FILE) {
				if (selectedFile.exists() && (!selectedFile.isFile())) {
					String title = Resources.translate("fileSaveError");
					PointCloudDialog.popup(title, message);
					return null;
				}
			}
			return selectedFile.getAbsolutePath();
		}
		return null;
	}
	/**
	 * Shows an input dialog box to request all parameters for the clustering
	 * algorithm. The values of the input fields can then be requested with the
	 * provided getter methods in this class. This method returns true if the
	 * OK button was pressed and the input was valid. If the input was invalid,
	 * the dialog box is shown again. If the user clicked CANCEL, this method
	 * returns false.
	 *
	 * @return true If the user clicked OK and false if the user clicked CANCEL
	 */
	public boolean requestClusteringParameters() {
		this.resetClusteringParameters();
		String title = Resources.translate("pointCloudClustering");
		JPanel panel = this.getClusteringDialogContentPanel();
		int buttons = JOptionPane.OK_CANCEL_OPTION;
		int type = JOptionPane.QUESTION_MESSAGE;
		int result;
		http://www.grogra.de
		do {
			result = JOptionPane.showConfirmDialog(null, panel, title, buttons, type);
			if (result == JOptionPane.CANCEL_OPTION) {
				return false;
			}
			if (this.getDistance() == Double.MIN_VALUE) {
				continue http;
			} else if (this.getMinimumNeighbors() == Integer.MIN_VALUE) {
				continue http;
			} else if (this.getOctreeDepth() == Integer.MIN_VALUE) {
				continue http;
			}
			break http;
		} while (true);
		return true;
	}
	/**
	 * Shows an input dialog box to request all parameters for the fitting
	 * algorithm. The values of the input fields can then be requested with the
	 * provided getter methods in this class. This method returns true if the
	 * OK button was pressed and the input was valid. If the input was invalid,
	 * the dialog box is shown again. If the user clicked CANCEL, this method
	 * returns false. The usePrecision parameter must be set to true for
	 * cylinders, frustums and cones, because these objects use the fibonacci
	 * sphere algorithm for the fitting. If a sphere should be fitted, it must
	 * be set to false because a sphere is not fitted with the fibonacci sphere
	 * algorithm and needs no precision parameter.
	 *
	 * @param usePrecision This value must be set to true for cylinders,
	 * frustums and cones. It must be set to false for spheres
	 * @return true If the user clicked OK and false if the user clicked CANCEL
	 */
	public boolean requestFittingParameters(boolean usePrecision) {
		this.resetFittingParameters();
		String title = Resources.translate("pointCloudFitting");
		JPanel panel = this.getFittingDialogContentPanel(usePrecision);
		int buttons = JOptionPane.OK_CANCEL_OPTION;
		int type = JOptionPane.QUESTION_MESSAGE;
		int result;
		http://www.grogra.de
		do {
			result = JOptionPane.showConfirmDialog(null, panel, title, buttons, type);
			if (result == JOptionPane.CANCEL_OPTION) {
				return false;
			}
			if (this.getPrecision() == Double.MIN_VALUE) {
				continue http;
			}
			break http;
		} while (true);
		return true;
	}
	/**
	 * Initializes and returns the panel with all input fields and belonging
	 * labels for the cluster parameter dialog box. The values of the input
	 * fields are not reset.
	 *
	 * @return The panel that should be shown in the settings dialog box
	 */
	private JPanel getClusteringDialogContentPanel() {
		String pointDistance = Resources.translate("pointDistance");
		String minimumNeighbors = Resources.translate("minimumNeighbors");
		String octreeDepth = Resources.translate("octreeDepth");
		String removeNoiseCluster = Resources.translate("removeNoiseCluster");
		JLabel distanceLabel = new JLabel(pointDistance + ":");
		JLabel minimumNeighborsLabel = new JLabel(minimumNeighbors + ":");
		JLabel octreeDepthLabel = new JLabel(octreeDepth + ":");
		JLabel removeNoiseClusterLabel = new JLabel(removeNoiseCluster + ":");
		JPanel panel = new JPanel();
		int columns = 2;
		int rows = 4;
		GridLayout gridLayout = new GridLayout(rows, columns);
		panel.setLayout(gridLayout);
		panel.add(distanceLabel);
		panel.add(this.distanceField);
		panel.add(minimumNeighborsLabel);
		panel.add(this.minimumNeighborsField);
		panel.add(octreeDepthLabel);
		panel.add(this.octreeDepthField);
		panel.add(removeNoiseClusterLabel);
		panel.add(this.removeNoiseClusterField);
		return panel;
	}
	/**
	 * Initializes and returns the panel with all input fields and belonging
	 * labels for the fit parameter dialog box. The values of the input fields
	 * are not reset. The usePrecision parameter must be set to true for
	 * cylinders, frustums and cones, because these objects use the fibonacci
	 * sphere algorithm for the fitting. If a sphere should be fitted, it must
	 * be set to false because a sphere is not fitted with the fibonacci sphere
	 * algorithm and needs no precision parameter.
	 *
	 * @param usePrecision This value must be set to true for cylinders,
	 * frustums and cones. It must be set to false for spheres
	 * @return The panel that should be shown in the settings dialog box
	 */
	private JPanel getFittingDialogContentPanel(boolean usePrecision) {
		JPanel panel = new JPanel();
		int columns = 2;
		int rows = (usePrecision ? 3 : 3);
		GridLayout gridLayout = new GridLayout(rows, columns);
		panel.setLayout(gridLayout);
		if (usePrecision) {
			String precision = Resources.translate("precision");
			JLabel precisionLabel = new JLabel(precision + ":");
			panel.add(precisionLabel);
			panel.add(this.precisionField);
		}
		String fitMaximum = Resources.translate("fitMaximum");
		String fitAverage = Resources.translate("fitAverage");
		JLabel maximumOptionLabel = new JLabel(fitMaximum);
		JLabel averageOptionLabel = new JLabel(fitAverage);
		panel.add(maximumOptionLabel);
		panel.add(this.fittingOptionMaximumField);
		panel.add(averageOptionLabel);
		panel.add(this.fittingOptionAverageField);
		return panel;
	}
	/**
	 * Returns the distance that was set by the last cluster parameter dialog.
	 * If the distance is no valid floating point number, an alert box is shown
	 * and the minimum value of the double data type is returned.
	 *
	 * @return The distance that was set by the last cluster parameter dialog or
	 * the minimum double value in case of an input error
	 */
	public double getDistance() {
		try {
			return Double.parseDouble(this.distanceField.getText());
		} catch (NumberFormatException numberFormatException) {
			String title = Resources.translate("inputError");
			String message = Resources.translate("distanceMustBeFloatError");
			PointCloudDialog.popup(title, message);
			return Double.MIN_VALUE;
		}
	}
	/**
	 * Returns the minimum number of neighbors that was set by the last cluster
	 * parameter dialog. If the minimum number of neighbors is no valid integer
	 * number, an alert box is shown and the minimum value of the integer data
	 * type is returned.
	 *
	 * @return The minimum number of neighbors that was set by the last cluster
	 * parameter dialog or the minimum integer value in case of an input error
	 */
	public int getMinimumNeighbors() {
		try {
			return Integer.parseInt(this.minimumNeighborsField.getText());
		} catch (NumberFormatException numberFormatException) {
			String title = Resources.translate("inputError");
			String message = Resources.translate("minimumNeighborsMustBeIntegerError");
			PointCloudDialog.popup(title, message);
			return Integer.MIN_VALUE;
		}
	}
	/**
	 * Returns the octree depth that was set by the last cluster parameter
	 * dialog. If the octree depth is no valid integer number, an alert box is
	 * shown and the minimum value of the integer data type is returned.
	 *
	 * @return The minimum number of neighbors that was set by the last cluster
	 * parameter dialog or the minimum integer value in case of an input error
	 */
	public int getOctreeDepth() {
		try {
			return Integer.parseInt(this.octreeDepthField.getText());
		} catch (NumberFormatException numberFormatException) {
			String title = Resources.translate("inputError");
			String message = Resources.translate("octreeDepthMustBeIntegerError");
			PointCloudDialog.popup(title, message);
			return Integer.MIN_VALUE;
		}
	}
	/**
	 * Returns true if the remove-noise-cluster checkbox was checked in the
	 * last cluster parameter dialog. Otherwise, false is returned.
	 *
	 * @return true If the remove-noise-cluster checkbox was checked, otherwise
	 * false
	 */
	public boolean shouldRemoveNoiseCluster() {
		return this.removeNoiseClusterField.isSelected();
	}
	/**
	 * Returns the precision that was set by the last fit parameter dialog. If
	 * the precision is no valid integer number, an alert box is shown and the
	 * minimum value of the integer data type is returned.
	 *
	 * @return The precision that was set by the last fit parameter dialog or
	 * the minimum integer value in case of an input error
	 */
	public int getPrecision() {
		try {
			return Integer.parseInt(this.precisionField.getText());
		} catch (NumberFormatException numberFormatException) {
			String title = Resources.translate("inputError");
			String message = Resources.translate("precisionMustBeIntegerError");
			PointCloudDialog.popup(title, message);
			return Integer.MIN_VALUE;
		}
	}
	/**
	 * Returns true if the average fitting option was selected in the radio
	 * button group in the fitting algorithm parameters dialog and false if the
	 * maximum option was selected there. 
	 *
	 * @return true If the average fitting option was selected and false if the
	 * maximum fitting option was selected
	 */
	public boolean shouldUseAverageFittingOption() {
		// This decision works because this.fittingOptionMaximumField and
		// this.fittingOptionAverageField are contained in
		// this.fittingOptionButtonGroup. This ensures that one of both is
		// always selected.
		return this.fittingOptionAverageField.isSelected();
	}
}