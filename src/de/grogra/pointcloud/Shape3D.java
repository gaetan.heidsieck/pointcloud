/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import java.util.List;
/**
 * This class represents a 3d shape. It is the super class for the box, the
 * cone, the cylinder, the frustum and the sphere in this package. This class is
 * used for the point cloud fitting feature because it is much more simple to
 * use than the original classes. It contains a position vector, a direction
 * vector and other metadata that is not directly contained in the original
 * objects. All 3d shapes have also the possibility to generate a score. With
 * that score, they can be compared to point clouds.
 *
 * @author Maurice Mueller
 */
public abstract class Shape3D {
	/**
	 * The position of this 3d shape
	 */
	private Vector position;
	/**
	 * The cylinder of this 3d shape
	 */
	private Vector direction;
	/**
	 * Creates a 3d shape with a position and a direction vector. This
	 * constructor should only be used by the extending classes with the super()
	 * call.
	 *
	 * @param position The position vector for this 3d shape
	 * @param direction The direction vector for this 3d shape
	 */
	protected Shape3D(Vector position, Vector direction) {
		this.position = position;
		this.direction = direction;
	}
	/**
	 * Sets the position of this cylinder.
	 *
	 * @param position The new position for this 3d shape
	 */
	public void setPosition(Vector position) {
		this.position = position;
	}
	/**
	 * Returns the position of this 3d shape.
	 *
	 * @return The position of this 3d shape
	 */
	public Vector getPosition() {
		return this.position;
	}
	/**
	 * Sets the direction of this 3d shape.
	 *
	 * @param direction The new direction for this 3d shape
	 */
	public void setDirection(Vector direction) {
		this.direction = direction;
	}
	/**
	 * Returns the direction of this 3d shape.
	 *
	 * @return The direction of this 3d shape
	 */
	public Vector getDirection() {
		return this.direction;
	}
	/**
	 * Calculates and returns the score of this 3d shape, compared with the
	 * given point cloud.
	 *
	 * @deprecated This feature is deprecated because all relevant fitting
	 * functions are using the volume instead of the score.
	 *
	 * @param pointCloud The point cloud to calculate the score for
	 * @return The score of this object, compared with the given point cloud
	 */
	@Deprecated
	public double getScore(PointCloud pointCloud) {
		List<Point> points = pointCloud.getPoints();
		Point point;
		double score = 0.0;
		double distance;
		int index = 0;
		while (index < points.size()) {
			point = points.get(index);
			distance = this.getMinimumDistanceToSurface(point);
			score += (distance < 0 ? -distance : distance);
			index++;
		}
		return score;// / (double)(points.size());// + this.getVolume() + this.getSurface();
	}
	/**
	 * Calculates and returns the minimum distance of the given point to the
	 * surface of this 3d shape. This method must be implemented in all
	 * extending classes.
	 *
	 * @param point The point to get the minimum distance to the surface of this
	 * 3d shape for
	 * @return The minimum distance of the given point to the surface of this 3d
	 * shape
	 */
	public abstract double getMinimumDistanceToSurface(Point point);
	/**
	 * Calculates and returns the volume of this 3d shape. This method must be
	 * implemented in all extending classes.
	 *
	 * @return The volume of this object
	 */
	public abstract double getVolume();
	/**
	 * Calculates and returns the surface of this 3d shape. This method must be
	 * implemented in all extending classes.
	 *
	 * @return The surface of this object
	 */
	public abstract double getSurface();
}