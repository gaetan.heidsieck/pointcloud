/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * This class represents a point cloud. The point cloud can be clustered, split,
 * merged, imported and exported later. The point cloud consists of a list of
 * points and a color.
 *
 * @author Maurice Mueller
 */
public class PointCloud {
	/**
	 * The points of this point cloud. All points have the id of this point
	 * cloud.
	 */
	private List<Point> points;
	/**
	 * The color of this point cloud
	 */
	private Color color;
	/**
	 * A constructor to get an empty point cloud. The point cloud will have a
	 * default color and an empty list of points.
	 */
	public PointCloud() {
		this(new ArrayList<Point>());
	}
	/**
	 * A constructor to get a point cloud with a list of points. The color will
	 * be a default color.
	 *
	 * @param points The list of points for the point cloud
	 */
	public PointCloud(List<Point> points) {
		this.points = points;
		this.color = PointCloud.getDefaultPointCloudColor();
		PointCloud.setPointCloudOfPoints(this, this.points);
	}
	/**
	 * Returns the default point cloud color from the resources and returns it
	 * as java.awt.Color object.
	 *
	 * @return The default color for point clouds
	 */
	public static Color getDefaultPointCloudColor() {
		int red = Resources.getInt("defaultPointCloudColorRed");
		int green = Resources.getInt("defaultPointCloudColorGreen");
		int blue = Resources.getInt("defaultPointCloudColorBlue");
		return new Color(red, green, blue);
	}
	/**
	 * Sets a random color for this point cloud.
	 */
	public void setRandomColor() {
		Random random = new Random();
		int maximum = 256;
		int red = random.nextInt(maximum);
		int green = random.nextInt(maximum);
		int blue = random.nextInt(maximum);
		this.color = new Color(red, green, blue);
	}
	/**
	 * Returns the number of points in this point cloud.
	 *
	 * @return The number of points in this point cloud
	 */
	public int getNumberOfPoints() {
		return this.points.size();
	}
	/**
	 * Adds a point to this point cloud. The point cloud reference of the point
	 * is replaced with this point cloud.
	 *
	 * @param point The point to add
	 */
	public void addPoint(Point point) {
		this.points.add(point);
		point.setPointCloud(this);
	}
	/**
	 * Returns the list of points of this point cloud.
	 *
	 * @return The list of points in this point cloud
	 */
	public List<Point> getPoints() {
		return this.points;
	}
	/**
	 * Sets the color of this point cloud.
	 *
	 * @param color The new color for this point cloud
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	/**
	 * Returns the color of this point cloud.
	 *
	 * @return The color of this point cloud
	 */
	public Color getColor() {
		return this.color;
	}
	/**
	 * Returns the center position of this point cloud as a point object. This
	 * method does not return the point with the most-centric position, it
	 * returns a position with the average position of all points in the point
	 * cloud instead.
	 *
	 * @return The center position of this point cloud as a point object
	 */
	public Point getCenter() {
		double centerX = this.getCenterX();
		double centerY = this.getCenterY();
		double centerZ = this.getCenterZ();
		return new Point(centerX, centerY, centerZ);
	}
	/**
	 * Returns the center x position of this point cloud.
	 *
	 * @return The center x position of this point cloud
	 */
	public double getCenterX() {
		return this.getMinimumX() + (this.getMaximumX() - this.getMinimumX()) / 2;
	}
	/**
	 * Returns the center y position of this point cloud.
	 *
	 * @return The center y position of this point cloud
	 */
	public double getCenterY() {
		return this.getMinimumY() + (this.getMaximumY() - this.getMinimumY()) / 2;
	}
	/**
	 * Returns the center z position of this point cloud.
	 *
	 * @return The center z position of this point cloud
	 */
	public double getCenterZ() {
		return this.getMinimumZ() + (this.getMaximumZ() - this.getMinimumZ()) / 2;
	}
	/**
	 * Returns the minimum x position of this point cloud.
	 *
	 * @return The minimum x position of this point cloud
	 */
	public double getMinimumX() {
		return this.getExtremum('x', '-');
	}
	/**
	 * Returns the maximum x position of this point cloud.
	 *
	 * @return The maximum x position of this point cloud
	 */
	public double getMaximumX() {
		return this.getExtremum('x', '+');
	}
	/**
	 * Returns the minimum y position of this point cloud.
	 *
	 * @return The minimum y position of this point cloud
	 */
	public double getMinimumY() {
		return this.getExtremum('y', '-');
	}
	/**
	 * Returns the maximum y position of this point cloud.
	 *
	 * @return The maximum y position of this point cloud
	 */
	public double getMaximumY() {
		return this.getExtremum('y', '+');
	}
	/**
	 * Returns the minimum z position of this point cloud.
	 *
	 * @return The minimum z position of this point cloud
	 */
	public double getMinimumZ() {
		return this.getExtremum('z', '-');
	}
	/**
	 * Returns the maximum z position of this point cloud.
	 *
	 * @return The maximum z position of this point cloud
	 */
	public double getMaximumZ() {
		return this.getExtremum('z', '+');
	}
	/**
	 * Returns the extremum value of this point cloud. The requested extremum
	 * value is specified with the given dimension and the given operator. To
	 * get a minimum value, the operator must be '-'. To get a maximum value,
	 * the operator must be '+'. To get a minimum in x, y or z direction, the
	 * dimension parameter must be 'x', 'y' or 'z'.
	 *
	 * @param dimension Must be 'x', 'y' or 'z' to get the correct dimension
	 * @param operator Must be '-' to get a minimum or '+' to get a maximum
	 * @return The minimum or maximum value in the requested dimension
	 */
	public double getExtremum(char dimension, char operator) {
		if (this.points == null || this.points.size() == 0) {
			return 0;
		}
		boolean minimum = operator == '-';
		boolean maximum = operator == '+';
		double extremum = this.getValue(0, dimension);
		double value;
		int length = this.points.size();
		int index = 1;
		while (index < length) {
			value = this.getValue(index, dimension);
			if ((minimum && value < extremum) || (maximum && value > extremum)) {
				extremum = value;
			}
			index++;
		}
		return extremum;
	}
	/**
	 * Returns the x, y or z coordinate at the given index. The dimension
	 * parameter must be 'x', 'y' or 'z' to get the x, y or z coordinate. If the
	 * dimension parameter is wrong or the index is outside the point list, an
	 * exception is thrown.
	 *
	 * @param index The index of the returned point
	 * @param dimension The dimension (x, y, or z) of the returned point
	 * @return The dimension value of the requested point
	 * @throws IllegalArgumentException If the index is < 0, too high or the
	 * dimension parameter is invalid
	 */
	public double getValue(int index, char dimension) {
		if (dimension == 'x') {
			return this.points.get(index).getX();
		} else if (dimension == 'y') {
			return this.points.get(index).getY();
		} else if (dimension == 'z') {
			return this.points.get(index).getZ();
		} else {
			String message = Resources.translate("parameterError");
			throw new IllegalArgumentException(message);
		}
	}
	/**
	 * Returns the point that has the most far distance to the center position
	 * of this point cloud. If there are multiple points with the same distance,
	 * the first one is returned. If this point cloud has no points, null is
	 * returned. If this point cloud has exactly one point, it is returned. In
	 * case of two points, the first one is returned.
	 *
	 * @return The point with the most far distance to the center position of
	 * this point cloud
	 */
	public Point getPointWithMostFarDistanceToCenterPosition() {
		if (this.points == null || this.points.size() == 0) {
			return null;
		}
		Point center = this.getCenter();
		double radius = 0;
		Point mostFarPoint = this.points.get(0);
		// start comparison with point after point with index 0
		int index = 1;
		while (index < this.points.size()) {
			double distance = this.points.get(index).getDistanceToPoint(center);
			if (distance > radius) {
				radius = distance;
				mostFarPoint = this.points.get(index);
			}
			index++;
		}
		return mostFarPoint;
	}
	/**
	 * Sets the given point cloud as point cloud of all given points.
	 *
	 * @param pointCloud The point cloud to set in all points
	 * @param points The list of points where to set the point cloud
	 */
	public static void setPointCloudOfPoints(PointCloud pointCloud, List<Point> points) {
		int index = 0;
		while (index < points.size()) {
			if (pointCloud != null) {
				points.get(index).setPointCloud(pointCloud);
			} else {
				points.get(index).setPointCloud(null);
			}
			index++;
		}
	}
}