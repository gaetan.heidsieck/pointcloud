/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a vector for the cylinder fitting class. This class
 * contains a data array that can hold as many dimensions as required. Most of
 * the methods are object based, but there are also static methods that take one
 * or two vectors and return a third one. This class exists because the
 * implemented functions are much simpler to use than from the javax.vecmath
 * package.
 *
 * @author Maurice Mueller
 */
public class Vector {
	/**
	 * The data of this vector. The elements and the number of elements can not
	 * be changed afterwards.
	 */
	private final double[] data;
	/**
	 * Creates a new vector and sets the data. If the data is null, an exception
	 * is thrown.
	 *
	 * @param data The new data for this vector
	 * @throws NullPointerException If the data array is null
	 */
	public Vector(double[] data) {
		if (data == null) {
			String message = Resources.translate("nullVectorError");
			throw new NullPointerException(message);
		}
		this.data = data;
	}
	/**
	 * Returns the number of elements in this vector.
	 *
	 * @return The number of elements in this vector
	 */
	public int getNumberOfElemenents() {
		return this.data.length;
	}
	/**
	 * Sets the value of the element at the given position.
	 *
	 * @param index The position where the value should be set
	 * @param value The value that should be set at that position
	 */
	public void set(int index, double value) {
		this.data[index] = value;
	}
	/**
	 * Returns the value of the element at the given position.
	 *
	 * @param index The position from where the value should be returned
	 * @return The value at that position
	 */
	public double get(int index) {
		return this.data[index];
	}
	/**
	 * Returns the pythagorean length of this vector. This length considers all
	 * dimensions.
	 *
	 * @return The pythagorean length of this vector
	 */
	public double getPythagoreanLength() {
		double sum = 0;
		int index = 0;
		while (index < this.data.length) {
			sum += this.data[index] * this.data[index];
			index++;
		}
		return Math.sqrt(sum);
	}
	/**
	 * Adds the values of both vectors and returns a third vector with the sum
	 * of both input vectors in each field of the output vector. If the
	 * dimensions do not fit together, an exception is thrown.
	 *
	 * @param vector1 The first vector for the sum
	 * @param vector2 The second vector for the sum
	 * @return The resulting vector with the sum of the elements from both input
	 * vectors
	 * @throws IllegalArgumentException If the vectors have a different number
	 * of elements
	 */
	public static Vector addVectors(Vector vector1, Vector vector2) {
		Vector.checkEqualLength(vector1, vector2);
		double[] result = new double[vector1.data.length];
		int index = 0;
		while (index < result.length) {
			result[index] = vector1.data[index] + vector2.data[index];
			index++;
		}
		return new Vector(result);
	}
	/**
	 * Subtracts the values of both vectors and returns a third vector with the
	 * subtraction of the second vector from the first vector in each field of
	 * the output vector. If the dimensions do not fit together, an exception is
	 * thrown.
	 *
	 * @param vector1 The first vector for the subtraction
	 * @param vector2 The second vector for the subtraction
	 * @return The resulting vector with the subtraction of the elements from
	 * both input vectors
	 * @throws IllegalArgumentException If the vectors have a different number
	 * of elements
	 */
	public static Vector subtractVectors(Vector vector1, Vector vector2) {
		Vector.checkEqualLength(vector1, vector2);
		double[] result = new double[vector1.data.length];
		int index = 0;
		while (index < result.length) {
			result[index] = vector1.data[index] - vector2.data[index];
			index++;
		}
		return new Vector(result);
	}
	/**
	 * Calculates and returns the scalar product of both given vectors and
	 * throws an exception if the dimensions of both input vectors are
	 * different.
	 *
	 * @param vector1 The first vector for the scalar product
	 * @param vector2 The second vector for the scalar product
	 * @return The scalar product of both input vectors
	 * @throws IllegalArgumentException If the dimensions of the vectors do not
	 * fit together
	 */
	public static double getScalarProduct(Vector vector1, Vector vector2) {
		Vector.checkEqualLength(vector1, vector2);
		double scalar = 0;
		int index = 0;
		while (index < vector1.data.length) {
			scalar += vector1.data[index] * vector2.data[index];
			index++;
		}
		return scalar;
	}
	/**
	 * Calculates and returns the cross product of two three dimensional
	 * vectors. If the vectors have too few dimensions, an exception will be
	 * thrown.
	 *
	 * @param vector1 The first vector for the cross product
	 * @param vector2 The second vector for the cross product
	 * @return The cross product of both vectors
	 */
	public static Vector getCrossProduct3(Vector vector1, Vector vector2) {
		double x = vector1.data[1] * vector2.data[2] - vector1.data[2] * vector2.data[1];
		double y = vector1.data[0] * vector2.data[2] - vector1.data[2] * vector2.data[0];
		double z = vector1.data[0] * vector2.data[1] - vector1.data[1] * vector2.data[0];
		return new Vector(new double[]{x, y, z});
	}
	/**
	 * Multiplies the given scalar value with all values of the given vector
	 * and returns a new vector with all multiplication results as fields.
	 *
	 * @param vector The vector to get the elements from
	 * @param scalar The scalar value to multiply with all vector values
	 * @return The new vector with all multiplication results
	 */
	public static Vector multiplyWithScalar(Vector vector, double scalar) {
		double[] newData = new double[vector.data.length];
		int index = 0;
		while (index < newData.length) {
			newData[index] = scalar * vector.data[index];
			index++;
		}
		return new Vector(newData);
	}
	/**
	 * Changes this vector so that the relation between the dimensions (the
	 * direction) are/is kept and the new length is as long as the given length.
	 *
	 * @param length The target length for this vector
	 */
	public void trimToLength(double length) {
		double oldLength = this.getPythagoreanLength();
		double ratio = length / oldLength;
		int index = 0;
		while (index < this.data.length) {
			this.data[index] *= ratio;
			index++;
		}
	}
	/**
	 * Clones this vector and returns the clone vector.
	 *
	 * @return The clone vector
	 */
	@Override
	public Vector clone() {
		double[] cloneData = new double[this.data.length];
		int index = 0;
		while (index < this.data.length) {
			cloneData[index] = this.data[index];
			index++;
		}
		return new Vector(cloneData);
	}
	/**
	 * Checks whether the given vectors have the same number of dimensions and
	 * throws an exception if they have a different number of dimensions.
	 *
	 * @param vector1 The first vector for the comparison
	 * @param vector2 The second vector for the comparison
	 * @throws IllegalArgumentException If the vectors have different numbers of
	 * dimensions
	 */
	private static void checkEqualLength(Vector vector1, Vector vector2) {
		if (vector1.data.length != vector2.data.length) {
			String message = Resources.translate("vectorLengthError");
			StringBuffer buffer = new StringBuffer();
			buffer.append(message);
			buffer.append(" (");
			buffer.append(vector1.data.length);
			buffer.append(" != ");
			buffer.append(vector2.data.length);
			buffer.append(")");
			throw new IllegalArgumentException(buffer.toString());
		}
	}
}