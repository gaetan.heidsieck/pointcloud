/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a cone. This class is used for the point cloud fitting
 * feature because it is much more simple to use than the original class.
 *
 * @author Maurice Mueller
 */
public class Cone extends Shape3D {
	/**
	 * The base radius of this cone
	 */
	private double baseRadius;
	/**
	 * The length of this cone
	 */
	private double length;
	/**
	 * Creates a cone object with a position, a direction, a base radius and a
	 * length.
	 *
	 * @param position The position for this cone
	 * @param direction The direction for this cone
	 * @param radius The base radius for this cone
	 * @param length The length for this cone
	 */
	public Cone(Vector position, Vector direction, double radius, double length) {
		super(position, direction);
		this.baseRadius = radius;
		this.length = length;
	}
	/**
	 * Sets the base radius of this cone.
	 *
	 * @param radius The new base radius for this cone
	 */
	public void setBaseRadius(double radius) {
		this.baseRadius = radius;
	}
	/**
	 * Returns the base radius of this cone.
	 *
	 * @return The base radius of this cone
	 */
	public double getBaseRadius() {
		return this.baseRadius;
	}
	/**
	 * Sets the length of this cone.
	 *
	 * @param length The new length for this cone
	 */
	public void setLength(double length) {
		this.length = length;
	}
	/**
	 * Returns the length of this cone.
	 *
	 * @return The length of this cone
	 */
	public double getLength() {
		return this.length;
	}
	/**
	 * Calculates and returns the minimum distance of the given point to the
	 * surface of this cone.
	 *
	 * WARNING: If a point is below the cone, only the distance to the base
	 * surface is returned. The distance may then be wrong!
	 *
	 * @param point The point to get the minimum distance to the surface of this
	 * cone for
	 * @return The minimum distance of the given point to the surface of this
	 * cone
	 */
	public double getMinimumDistanceToSurface(Point point) {
		// The distance must be calculated for the base and for the circular
		// surface individually. Then, the minimum distance is returned
		Vector normal = this.getDirection().clone();
		normal.trimToLength(this.length);
		Vector bottomPosition = this.getPosition();
		double distanceBase = point.getSignedDistanceToPlane(bottomPosition, normal);
		// If the point is below the cone, the distance to the circular surface
		// is not realistic anymore. Then, the positive distance to the bottom
		// surface is returned.
		if (distanceBase < 0) {
			return -distanceBase;
		}
		// Here, the distanceBase is always positive or 0
		// If the point is above the cone, the distance to the tip (top point of
		// the cone) is calculated with pythagorean theorem
		if (distanceBase > this.length) {
			Vector top = Vector.addVectors(this.getPosition(), this.getDirection());
			Vector topToPoint = Vector.subtractVectors(point.toVector(), top);
			return topToPoint.getPythagoreanLength();
		}
		// Here, the distanceBase is between 0 and length. The distance to the
		// relational radius of the cone can be calculated now. The minimum of
		// the distance to the cylindrical surface and the distance to the base
		// surface is then returned.
		double heightDependentRadius = this.baseRadius * (distanceBase / this.length);
		double distanceSide = point.getDistanceToLine(bottomPosition, normal);
		distanceSide = Math.abs(heightDependentRadius - distanceSide);
		if (distanceBase < distanceSide) {
			return distanceBase;
		} else {
			return distanceSide;
		}
	}
	/**
	 * Calculates and returns the volume of this cone.
	 *
	 * @return The volume of this cone
	 */
	public double getVolume() {
		return Math.PI * this.baseRadius * this.baseRadius * this.length / 3.0;
	}
	/**
	 * Calculates and returns the surface of this cone.
	 *
	 * @return The surface of this cone
	 */
	public double getSurface() {
		double diagonal = Math.sqrt(this.baseRadius * this.baseRadius + this.length * this.length);
		return Math.PI * this.baseRadius * (this.baseRadius + diagonal);
	}
}