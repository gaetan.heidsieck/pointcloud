/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp.IMP;
import de.grogra.imp3d.objects.ColoredNull;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.Plane;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.View3D;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.edit.GraphSelection;
import de.grogra.pf.ui.edit.Selection;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Workbench;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
/**
 * This class provides lots of point cloud relating features. All features can
 * be used in XL code an in the graphical user interface. In general, all
 * functions in this class that have a warning concerning reflection, should not
 * be removed. The methods that have a context parameter, an info parameter and
 * an item parameter are automatically called by the menu in the graphical user
 * interface of GroIMP. The methods below (with similar names, but with concrete
 * objects as parameters and return values) can be used in XL scripts. They are
 * always compatible to the objects in de.grogra.imp3d.objects. The fitting
 * methods exist twice. This makes it possible to use array of point clouds and
 * single point clouds. Furthermore, this class contains lots of helper methods
 * that can be used in other parts of GroIMP and in XL scripts. All functions
 * that have to do with the graphical user interface are using the javax.swing
 * dialog classes to interact with the user.
 *
 * @author Maurice Mueller
 */
public abstract class PointCloudTools {
	/**
	 * This method is called after the user has clicked on the import button in
	 * the point cloud menu. This method requests for a file and calls the
	 * internal method to import one or more point clouds from the specified
	 * file.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiImportFromFile(Item item, Object information, Context context) {
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		String file = dialog.selectFile(PointCloudDialog.OPEN_FILE);
		if (file == null) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.importFromFile(file);
		PointCloudTools.addNodesToTree(array, context);
	}
	/**
	 * This method is called after the user has clicked on the cluster button in
	 * the point cloud menu. This method requests for several cluster parameters
	 * and calls the internal method to cluster a point cloud.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiCluster(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		String title = Resources.translate("couldNotClusterPointCloudError");
		String message = Resources.translate("selectPointCloud");
		if (objects.size() != 1) {
			PointCloudDialog.popup(title, message);
			return;
		}
		if (!(objects.get(0) instanceof de.grogra.imp3d.objects.PointCloud)) {
			PointCloudDialog.popup(title, message);
			return;
		}
		de.grogra.imp3d.objects.PointCloud pointCloud;
		pointCloud = (de.grogra.imp3d.objects.PointCloud)(objects.get(0));
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestClusteringParameters();
		if (!success) {
			return;
		}
		int keep = PointCloudDialog.keepRemoveCancelPopup(PointCloudDialog.CLUSTER);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		double epsilon = dialog.getDistance();
		int minimumNeighbors = dialog.getMinimumNeighbors();
		int octreeDepth = dialog.getOctreeDepth();
		boolean removeNoiseCluster = dialog.shouldRemoveNoiseCluster();
		de.grogra.imp3d.objects.PointCloud[] clusters;
		clusters = PointCloudTools.cluster(pointCloud, epsilon, minimumNeighbors, octreeDepth, removeNoiseCluster);
		PointCloudTools.addNodesToTree(clusters, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * This method is called after the user has clicked on the split button in
	 * the point cloud menu. This method checks that exactly one point cloud and
	 * one plane are selected and calls the internal method to split a point
	 * cloud.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiSplit(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		String title = Resources.translate("couldNotSplitPointCloudError");
		String message = Resources.translate("selectPointCloudAndPlane");
		if (objects.size() != 2) {
			PointCloudDialog.popup(title, message);
			return;
		}
		de.grogra.imp3d.objects.PointCloud pointCloud = null;
		Plane plane = null;
		if (objects.get(0) instanceof de.grogra.imp3d.objects.PointCloud && objects.get(1) instanceof Plane) {
			pointCloud = (de.grogra.imp3d.objects.PointCloud)(objects.get(0));
			plane = (Plane)(objects.get(1));
		} else if (objects.get(0) instanceof Plane && objects.get(1) instanceof de.grogra.imp3d.objects.PointCloud) {
			pointCloud = (de.grogra.imp3d.objects.PointCloud)(objects.get(1));
			plane = (Plane)(objects.get(0));
		} else {
			PointCloudDialog.popup(title, message);
			return;
		}
		int keep = PointCloudDialog.keepRemoveCancelPopup(PointCloudDialog.SPLIT);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] clusters;
		clusters = PointCloudTools.split(pointCloud, plane);
		PointCloudTools.addNodesToTree(clusters, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * This method is called after the user has clicked on the merge button in
	 * the point cloud menu. This method checks that only point clouds are
	 * selected and at least two point clouds are selected and calls the
	 * internal method to cluster multiple point clouds.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiMerge(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 2)) {
			return;
		}
		int keep = PointCloudDialog.keepRemoveCancelPopup(PointCloudDialog.MERGE);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		de.grogra.imp3d.objects.PointCloud cloud = PointCloudTools.merge(array);
		PointCloudTools.addNodeToTree(cloud, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * This method is called after the user has clicked on the export button in
	 * the point cloud menu. This method requests for a file and calls the
	 * internal method to export one or more point clouds to the specified file.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiExportToFile(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		String file = dialog.selectFile(PointCloudDialog.SAVE_FILE);
		if (file == null) {
			return;
		}
		File fileObject = new File(file);
		if (fileObject.exists()) {
			String title = Resources.translate("overwriteFile");
			String message = Resources.translate("fileAlreadyExists");
			boolean overwriteFile = PointCloudDialog.confirm(title, message);
			if (!overwriteFile) {
				return;
			}
		}
		PointCloudTools.exportToFile(array, file);
	}
	/**
	 * Selects all point clouds in the current 3D view. This is useful when a
	 * point cloud was clustered and lots of point clouds were created. Then all
	 * point clouds can be selected automatically with this function. After
	 * that, the selection of point clouds can (for example) be exported.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiSelectAllPointClouds(Item item, Object information, Context context) {
		Node[] roots = PointCloudTools.getGraphRootNodes(context);
		List<Node> list = new ArrayList<Node>();
		int index = 0;
		while (index < roots.length) {
			List<Node> children = PointCloudTools.getNodesRecursively(roots[index]);
			int childIndex = 0;
			while (childIndex < children.size()) {
				Node child = children.get(childIndex);
				if (child instanceof de.grogra.imp3d.objects.PointCloud) {
					list.add(child);
				}
				childIndex++;
			}
			index++;
		}
		Node[] pointClouds = new Node[list.size()];
		index = 0;
		while (index < pointClouds.length) {
			pointClouds[index] = list.get(index);
			index++;
		}
		context.getWorkbench().select(pointClouds);
	}
	/**
	 * Generates / Fits a sphere for each point cloud in the current 3d view
	 * selection and adds the spheres to the 3d view. If the selection does not
	 * fit, an error dialog is shown. The point clouds can optionally be
	 * removed.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiFitSpheresToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestFittingParameters(false);
		if (!success) {
			return;
		}
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		boolean average = dialog.shouldUseAverageFittingOption();
		de.grogra.imp3d.objects.Sphere[] spheres;
		spheres = PointCloudTools.fitSpheresToPointClouds(array, average);
		PointCloudTools.addNodesToTree(spheres, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * Generates / Fits a cylinder for each point cloud in the current 3d view
	 * selection and adds the cylinders to the 3d view. If the selection does
	 * not fit, an error dialog is shown. The point clouds can optionally be
	 * removed.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiFitCylindersToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestFittingParameters(true);
		if (!success) {
			return;
		}
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		int precision = dialog.getPrecision();
		boolean average = dialog.shouldUseAverageFittingOption();
		de.grogra.imp3d.objects.Cylinder[] cylinders;
		cylinders = PointCloudTools.fitCylindersToPointClouds(array, average, precision);
		PointCloudTools.addNodesToTree(cylinders, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * Generates / Fits a frustum for each point cloud in the current 3d view
	 * selection and adds the frustum to the 3d view. If the selection does not
	 * fit, an error dialog is shown. The point clouds can optionally be
	 * removed.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiFitFrustumsToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestFittingParameters(true);
		if (!success) {
			return;
		}
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		int precision = dialog.getPrecision();
		boolean average = dialog.shouldUseAverageFittingOption();
		de.grogra.imp3d.objects.Frustum[] frustums;
		frustums = PointCloudTools.fitFrustumsToPointClouds(array, average, precision);
		PointCloudTools.addNodesToTree(frustums, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * Generates / Fits a cone for each point cloud in the current 3d view
	 * selection and adds the cones to the 3d view. If the selection does not
	 * fit, an error dialog is shown. The point clouds can optionally be
	 * removed.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiFitConesToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestFittingParameters(true);
		if (!success) {
			return;
		}
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		int precision = dialog.getPrecision();
		boolean average = dialog.shouldUseAverageFittingOption();
		de.grogra.imp3d.objects.Cone[] cones;
		cones = PointCloudTools.fitConesToPointClouds(array, average, precision);
		PointCloudTools.addNodesToTree(cones, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	public static void guiFitMeshToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);

		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		int precision = dialog.getPrecision();
		boolean average = dialog.shouldUseAverageFittingOption();
		de.grogra.imp3d.objects.MeshNode[] mesh;
		mesh = PointCloudTools.fitMeshToPointClouds(array, average, precision);
		
		PointCloudTools.addNodesToTree(mesh, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	
	/**
	 * Generates / Fits an automatic object (sphere, cylinder, frustum or cone)
	 * for each point cloud in the current 3d view selection and adds the
	 * objects to the 3d view. If the selection does not fit, an error dialog is
	 * shown. The point clouds can optionally be removed. The algorithm that
	 * works in the background of this function returns the object for each
	 * point cloud with the best score. If multiple point clouds are selected,
	 * different kinds of objects may be generated.
	 *
	 * WARNING: Do not remove this method, even if it is not called by other
	 * java methods! This method is called by a button in the GroIMP menu (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void guiFitAutomaticObjectsToPointClouds(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
			return;
		}
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
		PointCloudDialog dialog = PointCloudDialog.getInstance();
		boolean success = dialog.requestFittingParameters(true);
		if (!success) {
			return;
		}
		int option = (array.length == 1 ? PointCloudDialog.FIT_ONE : PointCloudDialog.FIT_MULTIPLE);
		int keep = PointCloudDialog.keepRemoveCancelPopup(option);
		if (keep == PointCloudDialog.CANCEL) {
			return;
		}
		int precision = dialog.getPrecision();
		boolean average = dialog.shouldUseAverageFittingOption();
		Node[] nodes = PointCloudTools.fitAutomaticObjectsToPointClouds(array, average, precision);
		PointCloudTools.addNodesToTree(nodes, context);
		if (keep == PointCloudDialog.REMOVE) {
			PointCloudTools.removeSelectedObjectsFrom3DView(context);
		}
	}
	/**
	 * This method imports one or more point clouds from the specified file and
	 * returns them as array. If the point cloud file does not provide
	 * information about the point cloud id, all points are imported into the
	 * same point cloud. This method is made for the usage in XL code.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param file The absolute path to the file to import
	 * @return The array with imported point clouds
	 */
	public static de.grogra.imp3d.objects.PointCloud[] importFromFile(String file) {
		List<PointCloud> clouds = PointCloudTools.readPointCloudFile(file);
		int index = 0;
		while (index < clouds.size()) {
			clouds.get(index).setRandomColor();
			index++;
		}
		return PointCloudTools.exportPointCloudArray(clouds);
	}
	/**
	 * Clusters the given point cloud with the given parameters and returns an
	 * array of point clouds, representing the clusters. This method is made for
	 * the usage in XL code.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to cluster
	 * @param epsilon The maximum distance for two points that are in the same
	 * cluster
	 * @param minimumNeighbors The minimum number of neighbors that must exist
	 * for a point to be no noise-cluster point.
	 * @param octreeDepth The number of layers in the octree that is used to
	 * cluster the point cloud. This must be > 1 to decrease the runtime of the
	 * clustering algorithm.
	 * @param removeNoiseCluster The indicator that can be set to true to remove
	 * all points that are not explicitly in an other point cloud.
	 * @return The array of point clouds that represent the clusters
	 */
	public static de.grogra.imp3d.objects.PointCloud[] cluster(de.grogra.imp3d.objects.PointCloud pointCloud, double epsilon, int minimumNeighbors, int octreeDepth, boolean removeNoiseCluster) {
		PointCloud cloud = PointCloudTools.convertPointCloud(pointCloud);
		List<PointCloud> clusters = PointCloudTools.cluster(cloud, epsilon, minimumNeighbors, octreeDepth, removeNoiseCluster);
		return PointCloudTools.exportPointCloudArray(clusters);
	}
	/**
	 * Splits the given point cloud on the given plane and returns both
	 * clusters. The clusters are stored in an array with two non-null point
	 * clouds. If the plane does not cut the point cloud, one of the resulting
	 * point clouds will be empty. This method is made for the usage in XL code.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to split
	 * @param plane The plane to split the point cloud on
	 * @return The array with two point clouds, representing the clusters
	 */
	public static de.grogra.imp3d.objects.PointCloud[] split(de.grogra.imp3d.objects.PointCloud pointCloud, Plane plane) {
		PointCloud front = new PointCloud();
		PointCloud back = new PointCloud();
		PointCloud origin = PointCloudTools.convertPointCloud(pointCloud);
		List<Point> points = origin.getPoints();
		int index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			if (PointCloudTools.isPointOverPlane(point, plane)) {
				front.addPoint(point);
			} else {
				back.addPoint(point);
			}
			index++;
		}
		de.grogra.imp3d.objects.PointCloud frontCloud;
		frontCloud = PointCloudTools.convertPointCloud(front);
		de.grogra.imp3d.objects.PointCloud backCloud;
		backCloud = PointCloudTools.convertPointCloud(back);
		return new de.grogra.imp3d.objects.PointCloud[]{frontCloud, backCloud};
	}
	/**
	 * Merges the given point clouds and returns one point cloud that contains
	 * all points of all given point clouds. This method is made for the usage
	 * in XL code.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The point clouds to merge
	 * @return A new point cloud that contains all points of all given point
	 * clouds
	 */
	public static de.grogra.imp3d.objects.PointCloud merge(de.grogra.imp3d.objects.PointCloud[] pointClouds) {
		PointCloud result = new PointCloud();
		int index = 0;
		while (index < pointClouds.length) {
			PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointClouds[index]);
			result.getPoints().addAll(internalPointCloud.getPoints());
			index++;
		}
		return PointCloudTools.convertPointCloud(result);
	}
	/**
	 * Exports the given point clouds to the given file. The file will contain
	 * the information about all points of all point clouds and the point cloud
	 * id for each point (the index in the pointClouds array). This method is
	 * made for the usage in XL code.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The point clouds to export to the file
	 * @param file The file to write the point clouds to
	 */
	public static void exportToFile(de.grogra.imp3d.objects.PointCloud[] pointClouds, String file) {
		List<PointCloud> clouds = PointCloudTools.importPointCloudArray(pointClouds);
		PointCloudTools.writePointCloudFile(file, clouds);
	}
	/**
	 * Generates / Fits an array of spheres to the given array of point clouds
	 * and returns it. This method can be called from XL code and is fully
	 * compatible to the 3D objects in de.grogra.imp3d.objects. The average
	 * parameter can be set to true to fit the point cloud so that a regression
	 * is executed for all points and the average radius is used. If the average
	 * parameter is set to false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The array of point clouds to fit the spheres to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @return The spheres that were generated fitting to the point clouds
	 */
	public static de.grogra.imp3d.objects.Sphere[] fitSpheresToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average) {
		de.grogra.imp3d.objects.Sphere[] spheres = new de.grogra.imp3d.objects.Sphere[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			spheres[index] = PointCloudTools.fitSphereToPointCloud(pointClouds[index], average);
			index++;
		}
		return spheres;
	}
	/**
	 * Generates / Fits a sphere to the given point cloud and returns it. This
	 * method can be called from XL code and is fully compatible to the 3D
	 * objects in de.grogra.imp3d.objects. The average parameter can be set to
	 * true to fit the point cloud so that a regression is executed for all
	 * points and the average radius is used. If the average parameter is set to
	 * false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to fit the sphere to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @return The sphere that was generated fitting to the point cloud
	 */
	public static de.grogra.imp3d.objects.Sphere fitSphereToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		Sphere internalSphere = PointCloudFittingTools.fitSphereToPointCloud(internalPointCloud, average);
		return PointCloudFittingTools.convertSphere(internalSphere);
	}
	/**
	 * Generates / Fits an array of cylinders to the given array of point clouds
	 * and returns it. This method can be called from XL code and is fully
	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
	 * precision parameter, the number of points in the fibonacci sphere can be
	 * specified. The average parameter can be set to true to fit the point
	 * cloud so that a regression is executed for all points and the average
	 * radius is used. If the average parameter is set to false, the maximum
	 * radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The array of point clouds to fit the cylinders to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The cylinders that were generated fitting to the point clouds
	 */
	public static de.grogra.imp3d.objects.Cylinder[] fitCylindersToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average, int precision) {
		de.grogra.imp3d.objects.Cylinder[] cylinders = new de.grogra.imp3d.objects.Cylinder[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			cylinders[index] = PointCloudTools.fitCylinderToPointCloud(pointClouds[index], average, precision);
			index++;
		}
		return cylinders;
	}
	/**
	 * Generates / Fits a cylinder to the given point cloud and returns it. This
	 * method can be called from XL code and is fully compatible to the 3D
	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
	 * number of points in the fibonacci sphere can be specified. The average
	 * parameter can be set to true to fit the point cloud so that a regression
	 * is executed for all points and the average radius is used. If the average
	 * parameter is set to false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to fit the cylinder to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The cylinder that was generated fitting to the point cloud
	 */
	public static de.grogra.imp3d.objects.Cylinder fitCylinderToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		Cylinder internalCylinder = PointCloudFittingTools.fitCylinderToPointCloud(internalPointCloud, average, precision);
		return PointCloudFittingTools.convertCylinder(internalCylinder);
	}
	/**
	 * Generates / Fits an array of frustums to the given array of point clouds
	 * and returns it. This method can be called from XL code and is fully
	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
	 * precision parameter, the number of points in the fibonacci sphere can be
	 * specified. The average parameter can be set to true to fit the point
	 * cloud so that a regression is executed for all points and the average
	 * radius is used. If the average parameter is set to false, the maximum
	 * radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The array of point clouds to fit the frustums to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The frustums that were generated fitting to the point clouds
	 */
	public static de.grogra.imp3d.objects.Frustum[] fitFrustumsToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average, int precision) {
		de.grogra.imp3d.objects.Frustum[] frustums = new de.grogra.imp3d.objects.Frustum[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			frustums[index] = PointCloudTools.fitFrustumToPointCloud(pointClouds[index], average, precision);
			index++;
		}
		return frustums;
	}
	/**
	 * Generates / Fits a frustum to the given point cloud and returns it. This
	 * method can be called from XL code and is fully compatible to the 3D
	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
	 * number of points in the fibonacci sphere can be specified. The average
	 * parameter can be set to true to fit the point cloud so that a regression
	 * is executed for all points and the average radius is used. If the average
	 * parameter is set to false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to fit the frustum to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The frustum that was generated fitting to the point cloud
	 */
	public static de.grogra.imp3d.objects.Frustum fitFrustumToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		Frustum internalFrustum = PointCloudFittingTools.fitFrustumToPointCloud(internalPointCloud, average, precision);
		return PointCloudFittingTools.convertFrustum(internalFrustum);
	}
	/**
	 * Generates / Fits an array of cones to the given array of point clouds and
	 * returns it. This method can be called from XL code and is fully
	 * compatible to the 3D objects in de.grogra.imp3d.objects. With the
	 * precision parameter, the number of points in the fibonacci sphere can be
	 * specified. The average parameter can be set to true to fit the point
	 * cloud so that a regression is executed for all points and the average
	 * radius is used. If the average parameter is set to false, the maximum
	 * radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The array of point clouds to fit the cones to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The cones that were generated fitting to the point clouds
	 */
	public static de.grogra.imp3d.objects.Cone[] fitConesToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average, int precision) {
		de.grogra.imp3d.objects.Cone[] cones = new de.grogra.imp3d.objects.Cone[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			cones[index] = PointCloudTools.fitConeToPointCloud(pointClouds[index], average, precision);
			index++;
		}
		return cones;
	}
	public static de.grogra.imp3d.objects.MeshNode[] fitMeshToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average, int precision) {
		de.grogra.imp3d.objects.MeshNode[] mesh = new de.grogra.imp3d.objects.MeshNode[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			mesh[index] = PointCloudTools.fitMeshToPointCloud(pointClouds[index], average, precision);
			index++;
		}
		return mesh;
	}
	
	/**
	 * Generates / Fits a cone to the given point cloud and returns it. This
	 * method can be called from XL code and is fully compatible to the 3D
	 * objects in de.grogra.imp3d.objects. With the precision parameter, the
	 * number of points in the fibonacci sphere can be specified. The average
	 * parameter can be set to true to fit the point cloud so that a regression
	 * is executed for all points and the average radius is used. If the average
	 * parameter is set to false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to fit the cone to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The cone that was generated fitting to the point cloud
	 */
	public static de.grogra.imp3d.objects.Cone fitConeToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		Cone internalCone = PointCloudFittingTools.fitConeToPointCloud(internalPointCloud, average, precision);
		return PointCloudFittingTools.convertCone(internalCone);
	}
	public static de.grogra.imp3d.objects.MeshNode fitMeshToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		//PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		MeshNode internalMesh = PointCloudFittingTools.fitMeshToPointCloud(pointCloud, average, precision);
		return PointCloudFittingTools.convertMesh(internalMesh);
	}

	/**
	 * Generates / Fits an array of automatically selected objects (spheres,
	 * cylinders, frustums and cones) to the given array of point clouds and
	 * returns it. This method can be called from XL code and is fully
	 * compatible to the 3D objects in de.grogra.imp3d.objects. If this method
	 * is used to fit multiple point clouds, multiple different types of objects
	 * can be returned. With the precision parameter, the number of points in
	 * the fibonacci sphere can be specified. The average parameter can be set
	 * to true to fit the point cloud so that a regression is executed for all
	 * points and the average radius is used. If the average parameter is set to
	 * false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointClouds The array of point clouds to fit the objects to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The objects that were generated fitting to the point clouds
	 */
	public static Node[] fitAutomaticObjectsToPointClouds(de.grogra.imp3d.objects.PointCloud[] pointClouds, boolean average, int precision) {
		Node[] nodes = new Node[pointClouds.length];
		int index = 0;
		while (index < pointClouds.length) {
			nodes[index] = PointCloudTools.fitAutomaticObjectToPointCloud(pointClouds[index], average, precision);
			index++;
		}
		return nodes;
	}
	/**
	 * Generates / Fits an automatically selected object (sphere, cylinder,
	 * frustum or cone) to the given point cloud and returns it. This method can
	 * be called from XL code and is fully compatible to the 3D objects in
	 * de.grogra.imp3d.objects. With the precision parameter, the number of
	 * points in the fibonacci sphere can be specified. The average parameter
	 * can be set to true to fit the point cloud so that a regression is
	 * executed for all points and the average radius is used. If the average
	 * parameter is set to false, the maximum radius of all points is used.
	 *
	 * WARNING: Do not remove this method, even if it is not used by other
	 * java methods! This method is made to be called in XL code (with
	 * reflection). Otherwise, a MethodNotFoundException will be thrown.
	 *
	 * @param pointCloud The point cloud to fit the object to
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @return The object that was generated fitting to the point cloud
	 */
	public static Node fitAutomaticObjectToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		Shape3D internalNode = PointCloudFittingTools.fitAutomaticObjectToPointCloud(internalPointCloud, average, precision);
		return PointCloudFittingTools.convertShape3D(internalNode);
	}
	/**
	 * Checks whether the given list of objects (the current selection in the 3d
	 * view) only contains point clouds of type
	 * de.grogra.imp3d.objects.PointCloud. The list must also contain at least
	 * 'minimum' point clouds. If the list contains enough point clouds of that
	 * type and no other objects, true is returned. If there are wrong objects
	 * or the list has too few objects, false is returned. In case of wrong
	 * objects or too few objects, an error message dialog window is shown. With
	 * the minimum parameter, the minimum number of expected point clouds can be
	 * specified. This method will fail if less than the given number of point
	 * clouds are in the selection (given object list).
	 *
	 * @param objects The list of objects (the current selection in the 3d view)
	 * @param minimum The minimum number of point clouds that is expected by the
	 * function that calls this method
	 * @return true If the list contains one or more points clouds and no wrong
	 * objects and false otherwise
	 */
	public static boolean checkPointCloudSelection(List<Object> objects, int minimum) {
		String title = Resources.translate("couldNotUsePointCloudsError");
		Class<?> reference = de.grogra.imp3d.objects.PointCloud.class;
		if (!PointCloudTools.contains(objects, reference, objects.size())) {
			String message = Resources.translate("unselectWrongObjects");
			PointCloudDialog.popup(title, message);
			return false;
		}
		if (objects.size() < minimum) {
			String message = Resources.translate("minimumPointCloudNumber");
			PointCloudDialog.popup(title, message + ": " + minimum);
			return false;
		}
		return true;
	}
	/**
	 * Returns the root nodes of the displayed object graph of the current 3D
	 * view in the given context. There are multiple root nodes in the graph,
	 * but normally only the first one (with index 0) is relevant. It contains
	 * an RGG root node as direct child element and contains the displayed
	 * objects generated by XL scripts.
	 *
	 * @param context The current context where the method is executed
	 * @return The array of root nodes of the displayed graph
	 */
	public static Node[] getGraphRootNodes(Context context) {
		View3D view3d = View3D.getDefaultView(context);
		Graph graph = view3d.getGraph();
		String[] keys = graph.getRootKeys();
		if (keys == null || keys.length == 0) {
			return new Node[0];
		}
		Node[] nodes = new Node[keys.length];
		int index = 0;
		while (index < keys.length) {
			Object object = graph.getRoot(keys[index]);
			Node node = (Node)(object);
			nodes[index] = node;
			index++;
		}
		return nodes;
	}
	/**
	 * Adds the nodes as child nodes to the RGG tree.
	 *
	 * @param nodes The nodes to add
	 * @param context The current context where the method is executed
	 */
	public static void addNodesToTree(Node[] nodes, Context context) {
		int index = 0;
		while (index < nodes.length) {
			PointCloudTools.addNodeToTree(nodes[index], context);
			index++;
		}
	}
	/**
	 * Adds a node as child node to the RGG tree.
	 *
	 * @param node The node to add
	 * @param context The current context where the method is executed
	 */
	public static void addNodeToTree(Node node, Context context) {
		IMP.addNode(null, node, context);
	}
	/**
	 * Removes the selected objects from the graph tree in the 3D selection.
	 *
	 * @param context The context to get the selected objects from
	 */
	public static void removeSelectedObjectsFrom3DView(Context context) {
		Object selectionObject = UIProperty.WORKBENCH_SELECTION.getValue(context);
		boolean isSelection = selectionObject instanceof Selection;
		if (!isSelection) {
			return;
		}
		Selection selection = (Selection)(selectionObject);
		int capabilities = selection.getCapabilities();
		if ((capabilities & Selection.DELETABLE) != 0) {
			selection.delete(false);
		}
	}
	/**
	 * Removes the objects that are given in the node array from the graph tree
	 * in the 3D view. This method is implemented a bit strangely, but it was
	 * the easiest solution. Because only objects from the current selection can
	 * be removed from the current 3D view, this method uses the current
	 * selection for the remove operation. Therefore, the current content of the
	 * selection is cached in an own node list. Then, the selection is set to
	 * the list of objects that should be removed. This makes it possible to
	 * remove the targeted objects. After the remove operation, a new list of
	 * objects is created where all objects from the previous selection are
	 * contained, but without the objects that are removed in between. The newly
	 * created list of nodes is then selected to reconstruct the selection from
	 * before.
	 *
	 * @param context The context to remove the objects in
	 * @param objects The array of node objects that should be removed from the
	 * 3D view
	 */
	public static void removeObjectsFrom3DView(Context context, Node[] objects) {
		// Sorry for the dirty hack, but it seems to be the best solution :-/
		if (!(context instanceof Workbench)) {
			return;
		}
		Workbench workbench = (Workbench)(context);
		// step 1: get objects from current selection
		List<Object> oldSelection = PointCloudTools.loadSelectedObjects(workbench);
		// step 2: load objects parameter into selection
		workbench.select(objects);
		// step 3: delete selection
		PointCloudTools.removeSelectedObjectsFrom3DView(workbench);
		// step 4: remove objects from old selection that have been removed
		List<Node> newSelection = new ArrayList<>();
		int index = 0;
		while (index < oldSelection.size()) {
			if (oldSelection.get(index) instanceof Node) {
				index++;
				continue;
			}
			boolean contained = false;
			int innerIndex = 0;
			innerLoop:
			while (innerIndex < objects.length) {
				if (objects[index] == oldSelection.get(index)) {
					contained = true;
					break innerLoop;
				}
				innerIndex++;
			}
			if (!contained) {
				newSelection.add((Node)(oldSelection.get(index)));
			}
			index++;
		}
		// step 5: put objects into array
		Node[] newObjectsToSelect = new Node[newSelection.size()];
		index = 0;
		while (index < newObjectsToSelect.length) {
			newObjectsToSelect[index] = newSelection.get(index);
			index++;
		}
		// step 6: load stored objects into selection
		workbench.select(newObjectsToSelect);
	}
	/**
	 * Removes all objects from the current 3D view.
	 *
	 * @param context The context to remove the objects in
	 */
	public static void removeAllObjectsFrom3DView(Context context) {
		Node[] roots = PointCloudTools.getGraphRootNodes(context);
		List<Node> nodes = new ArrayList<>();
		int index = 0;
		while (index < roots.length) {
			List<Node> subNodes = PointCloudTools.getNodesRecursively(roots[index]);
			int innerIndex = 0;
			while (innerIndex < subNodes.size()) {
				boolean isShaded = subNodes.get(innerIndex) instanceof ShadedNull;
				boolean isColored = subNodes.get(innerIndex) instanceof ColoredNull;
				if (isShaded || isColored) {
					nodes.add(subNodes.get(innerIndex));
				}
				innerIndex++;
			}
			index++;
		}
		Node[] nodeArray = new Node[nodes.size()];
		index = 0;
		while (index < nodeArray.length) {
			nodeArray[index] = nodes.get(index);
			index++;
		}
		PointCloudTools.removeObjectsFrom3DView(context, nodeArray);
	}
	/**
	 * Returns the a list with all direct and indirect child nodes of the given
	 * node. The root node is also contained in the list and is always at
	 * position 0.
	 *
	 * @param root The root node to recursively get all child elements from
	 * @return The list with all node objects that are direct or indirect child
	 * objects of the root node
	 */
	public static List<Node> getNodesRecursively(Node root) {
		List<Node> nodes = new ArrayList<Node>();
		if (root == null) {
			return nodes;
		}
		nodes.add(root);
		Node[] children = PointCloudTools.getChildNodes(root);
		int index = 0;
		while (index < children.length) {
			nodes.addAll(PointCloudTools.getNodesRecursively(children[index]));
			index++;
		}
		return nodes;
	}
	/**
	 * Returns the child nodes of a graph node as node array. Because this is
	 * very complicated with the normal methods, this method makes it easier to
	 * use.
	 *
	 * @param node The node to get the child nodes for
	 * @return The array of child nodes
	 */
	public static Node[] getChildNodes(Node node) {
		List<Edge> nodes = new ArrayList<Edge>();
		Edge edge = node.getFirstEdge();
		if (edge == null) {
			return new Node[0];
		}
		if (edge.isSource(node)) {
			nodes.add(edge);
		}
		while (true) {
			edge = edge.getNext(node);
			if (edge != null && edge.isSource(node)) {
				nodes.add(edge);
			} else if (edge == null) {
				break;
			}
		}
		Node[] array = new Node[nodes.size()];
		int index = 0;
		while (index < array.length) {
			array[index] = (Node)(nodes.get(index));
			index++;
		}
		return array;
	}
	/**
	 * Returns true if the given objects list contains exactly the given number
	 * of objects of the given type. Otherwise, false is returned.
	 *
	 * @param objects The object list to check the object count in
	 * @param clazz The class of the counted objects
	 * @param number The required number of objects in the list
	 * @return true I the objects list contains exactly the number of objects of
	 * the given type, otherwise false
	 */
	public static boolean contains(List<Object> objects, Class<?> clazz, int number) {
		int counter = 0;
		int index = 0;
		while (index < objects.size()) {
			if (clazz.isInstance(objects.get(index))) {
				counter++;
			}
			index++;
		}
		return counter == number;
	}
	/**
	 * Converts the list of objects to an array of point clouds. All objects in
	 * the given object list must be de.grogra.imp3d.objects.PointCloud
	 * objects. Otherwise, an exception is thrown.
	 *
	 * @param objects The list of point cloud objects to cast
	 * @return The array of point clouds
	 * @throws ClassCastException If other objects are contained in the objects
	 * list
	 */
	public static de.grogra.imp3d.objects.PointCloud[] secureCastPointClouds(List<Object> objects) {
		de.grogra.imp3d.objects.PointCloud[] pointClouds;
		pointClouds = new de.grogra.imp3d.objects.PointCloud[objects.size()];
		int index = 0;
		while (index < pointClouds.length) {
			pointClouds[index] = (de.grogra.imp3d.objects.PointCloud)(objects.get(index));
			index++;
		}
		return pointClouds;
	}
	/**
	 * Returns true if the point is on one side of the given plane and false if
	 * it is on the other side
	 *
	 * @param point The point to check the position of
	 * @param plane The plane to check whether the point is on the one side or
	 * on the other side
	 * @return true If the point is on one side of the plane and false otherwise
	 */
	public static boolean isPointOverPlane(Point point, Plane plane) {
		// matrix.m01, matrix.m02 and matrix.m03 are the normal vector
		Matrix4d matrix = plane.getLocalTransformation();
		Vector3d myPoint = new Vector3d(point.getX(), point.getY(), point.getZ());
		Vector3d position = plane.getTranslation();
		Vector3d normal = new Vector3d(matrix.m02, matrix.m12, matrix.m22);
		position.sub(myPoint);
		return position.dot(normal) > 0;
	}
	/**
	 * Clusters the given point cloud so that all points with a distance below
	 * the given epsilon parameter are in one cluster. The points with a number
	 * of neighbors lower than the given minimumNeighbors parameter are added to
	 * a noise cluster. The given octreeDepth parameter is used for the octree
	 * used in this clustering algorithm. The returned clusters are represented
	 * by point clouds and are returned as a list of point clouds. The noise
	 * cluster is added to the list if the removeNoiseCluster parameter is set
	 * to false. This makes it possible to use or remove it later reliably.
	 *
	 * @param cloud The point cloud to cluster
	 * @param epsilon The distance between to points to be neighbored
	 * @param minimumNeighbors The number of neighbored points that is required
	 * to let a point not be a noise point
	 * @param octreeDepth The used number of layers in the octree
	 * @param removeNoiseCluster Must be false to let the last returned cluster
	 * in the list be the cluster with all noise points
	 * @return The list of clusters. If the removeNoiseCluster parameter is set
	 * to false, the last cluster is always the noise cluster
	 */
	public static List<PointCloud> cluster(PointCloud cloud, double epsilon, int minimumNeighbors, int octreeDepth, boolean removeNoiseCluster) {
		List<PointCloud> clusters = new ArrayList<PointCloud>();
		Octree octree = PointCloudTools.createOctree(cloud, octreeDepth);
		PointCloud noise = new PointCloud();
		noise.setRandomColor();
		PointCloud.setPointCloudOfPoints(null, cloud.getPoints());
		int index = 0;
		while (index < cloud.getPoints().size()) {
			Point point = cloud.getPoints().get(index);
			if (point.getPointCloud() == null) {
				List<Point> neighborList = octree.getNeighbors(point, epsilon);
				if (neighborList.size() < minimumNeighbors) {
					point.setPointCloud(noise);
					noise.getPoints().add(point);
					index++;
					continue;
				}
				PointCloud cluster = new PointCloud();
				cluster.setRandomColor();
				List<Point> neighbors = new ArrayList<Point>();
				neighbors.add(point);
				PointCloudTools.addNeighborsToCluster(octree, cluster, neighbors, epsilon);
				clusters.add(cluster);
			}
			index++;
		}
		if (!removeNoiseCluster) {
			clusters.add(noise);
		}
		return clusters;
	}
	/**
	 * Searches for all points in the octree that are neighbored to points of
	 * the given neighbors list and adds all neighbored points to that list and
	 * the current point cloud. Points are neighbored if the distance (epsilon)
	 * between both points is lower than the given epsilon parameters.
	 *
	 * @param octree The octree to get the points from
	 * @param pointCloud The point cloud to put the points to
	 * @param neighbors The list of currently checked points
	 * @param epsilon The maximum distance that must be between two points to be
	 * neighbored
	 */
	private static void addNeighborsToCluster(Octree octree, PointCloud pointCloud, List<Point> neighbors, double epsilon) {
		if (neighbors.size() == 0) {
			return;
		}
		Point point = neighbors.get(0);
		List<Point> currentNeighbors = octree.getNeighbors(point, epsilon);
		neighbors.remove(0);
		pointCloud.addPoint(point);
		point.setPointCloud(pointCloud);
		int index = 0;
		while (index < currentNeighbors.size()) {
			Point neighbor = currentNeighbors.get(index);
			if (neighbor.getPointCloud() == null) {
				neighbor.setPointCloud(pointCloud);
				neighbors.add(neighbor);
				PointCloudTools.addNeighborsToCluster(octree, pointCloud, neighbors, epsilon);
			}
			index++;
		}
	}
	/**
	 * Creates and returns an octree of the given point cloud. All points of the
	 * point cloud are copied to the octree and are distributed to the given
	 * layer in the octree.
	 *
	 * @param cloud The point cloud to use the points from
	 * @param depth The number of layers for the octree
	 * @return The octree with all points of the given point cloud
	 */
	public static Octree createOctree(PointCloud cloud, int depth) {
		Octree octree = new Octree(0);
		octree.setPoints(PointCloudTools.copyPoints(cloud.getPoints()));
		octree.setMinimumX(cloud.getMinimumX());
		octree.setMinimumY(cloud.getMinimumY());
		octree.setMinimumZ(cloud.getMinimumZ());
		octree.setMaximumX(cloud.getMaximumX());
		octree.setMaximumY(cloud.getMaximumY());
		octree.setMaximumZ(cloud.getMaximumZ());
		octree.movePointsToLayer(depth);
		return octree;
	}
	/**
	 * Creates a new list of points and copies the given points to it. The new
	 * list is returned. The points are the same objects in both list. If a
	 * point is changed in one of the lists, it automatically affects the other
	 * list.
	 *
	 * @param points The list of points that should be copied
	 * @return The copy of the point list
	 */
	public static List<Point> copyPoints(List<Point> points) {
		List<Point> result = new ArrayList<Point>();
		int index = 0;
		while (index < points.size()) {
			result.add(points.get(index));
			index++;
		}
		return result;
	}
	/**
	 * Imports an array of graphically usable point clouds to a list of
	 * internally usable point clouds and returns the list.
	 *
	 * @param pointClouds The array of graphically usable point clouds
	 * @return The list of internally usable point clouds
	 */
	public static List<PointCloud> importPointCloudArray(de.grogra.imp3d.objects.PointCloud[] pointClouds) {
		List<PointCloud> list = new ArrayList<PointCloud>();
		int index = 0;
		while (index < pointClouds.length) {
			list.add(PointCloudTools.convertPointCloud(pointClouds[index]));
			index++;
		}
		return list;
	}
	/**
	 * Exports a list of internally usable point clouds to an array of
	 * graphically usable point clouds and returns the array.
	 *
	 * @param pointClouds The list of internally usable point clouds
	 * @return The array of graphically usable point clouds
	 */
	public static de.grogra.imp3d.objects.PointCloud[] exportPointCloudArray(List<PointCloud> pointClouds) {
		de.grogra.imp3d.objects.PointCloud[] array;
		array = new de.grogra.imp3d.objects.PointCloud[pointClouds.size()];
		int index = 0;
		while (index < array.length) {
			array[index] = PointCloudTools.convertPointCloud(pointClouds.get(index));
			index++;
		}
		return array;
	}
	/**
	 * Converts the given graphically usable point cloud object to an internal
	 * point cloud object and returns it. The point cloud color is copied to the
	 * returned point cloud.
	 *
	 * @param pointCloud The point cloud that is used for graphical purposes
	 * @return The internally usable point cloud
	 */
	public static PointCloud convertPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud) {
		float[] array = pointCloud.getPoints();
		List<Point> points = PointCloudTools.convertToPointList(array);
		Vector3d position = pointCloud.getTranslation();
		int index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			point.setX(point.getX() + position.x);
			point.setY(point.getY() + position.y);
			point.setZ(point.getZ() + position.z);
			index++;
		}
		PointCloud targetPointCloud = new PointCloud(points);
		try {
			targetPointCloud.setColor(pointCloud.getColor().get());
		} catch (Exception exception) {
		}
		return targetPointCloud;
	}
	/**
	 * Converts the given internal point cloud object to a graphically usable
	 * point cloud object and returns it. The point cloud color is copied to the
	 * returned point cloud.
	 *
	 * @param pointCloud The point cloud that was internally used for clustering
	 * @return The graphically usable point cloud
	 */
	public static de.grogra.imp3d.objects.PointCloud convertPointCloud(PointCloud pointCloud) {
		de.grogra.imp3d.objects.PointCloud targetPointCloud;
		targetPointCloud = new de.grogra.imp3d.objects.PointCloud();
		targetPointCloud.setColor(pointCloud.getColor().getRGB());
		targetPointCloud.setPointSize(3);
		List<Point> points = pointCloud.getPoints();
		if (points.size() > 0) {
			double minimumX = pointCloud.getMinimumX();
			double minimumY = pointCloud.getMinimumY();
			double minimumZ = pointCloud.getMinimumZ();
			Matrix4d matrix = targetPointCloud.getLocalTransformation();
			matrix.m03 = minimumX;
			matrix.m13 = minimumY;
			matrix.m23 = minimumZ;
			targetPointCloud.setTransform(matrix);
			int index = 0;
			while (index < points.size()) {
				Point point = points.get(index);
				point.setX(point.getX() - minimumX);
				point.setY(point.getY() - minimumY);
				point.setZ(point.getZ() - minimumZ);
				index++;
			}
		}
		targetPointCloud.setPoints(PointCloudTools.convertToFloatArray(points));
		return targetPointCloud;
	}
	/**
	 * This method seems to be a bit useless, but it uses the fact that the
	 * objects.PointCloud can be imported to transform the internal point
	 * position to an object position. For example, a point cloud has lots of
	 * points at (100/100/100). Then, the point cloud origin is still at
	 * (0/0/0). By using this function (and the import- and export process), the
	 * position of the point cloud is moved to (100/100/100) and the internal
	 * positions of the points are moved to (0/0/0). This has the advantage that
	 * the "translation arrows" in the 3D view are displayed directly at the
	 * point cloud, but the position of the points in the point clouds are
	 * globally unchanged. This method does not change the given point cloud,
	 * but returns the transformed point cloud.
	 *
	 * @param pointCloud The point cloud that should be normalized
	 * @return The transformed point cloud
	 */
	public static de.grogra.imp3d.objects.PointCloud normalizePointCloud(de.grogra.imp3d.objects.PointCloud pointCloud) {
		PointCloud internalPointCloud = PointCloudTools.convertPointCloud(pointCloud);
		return PointCloudTools.convertPointCloud(internalPointCloud);
	}
	/**
	 * Converts the points array to a point list and returns it. The point cloud
	 * id of all points is set to 0.
	 *
	 * @param points The array of point coordinates (consisting of x, y and z)
	 * @return The list of point objects
	 */
	public static List<Point> convertToPointList(float[] points) {
		List<Point> list = new ArrayList<Point>();
		int index = 0;
		while (index < points.length) {
			double x = points[index + 0];
			double y = points[index + 1];
			double z = points[index + 2];
		 	list.add(new Point(x, y, z));
			index += 3;
		}
		return list;
	}
	/**
	 * Converts the list of points to a float array and returns it. The
	 * information about point cloud ids gets lost in the float array. If this
	 * method is used for multiple point clouds, it should be called for each
	 * point cloud separately.
	 *
	 * @param points The list of points to convert to the float array
	 * @return The float array with all points
	 */
	public static float[] convertToFloatArray(List<Point> points) {
		float[] array = new float[3 * points.size()];
		int index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			array[3 * index + 0] = (float)(point.getX());
			array[3 * index + 1] = (float)(point.getY());
			array[3 * index + 2] = (float)(point.getZ());
			index++;
		}
		return array;
	}
	/**
	 * Returns a list that contains all objects of the RGG tree that are
	 * currently selected in the workbench. All root nodes are considered. If
	 * no objects are selected, an empty list is returned. The objects are not
	 * sorted by their tree structure.
	 *
	 * @param context The current context of the workbench
	 * @return The list of selected objects
	 */
	public static List<Object> loadSelectedObjects(Context context) {
		Node[] rootNodes = PointCloudTools.getGraphRootNodes(context);
		List<Object> list = new ArrayList<Object>();
		int index = 0;
		while (index < rootNodes.length) {
			Node root = rootNodes[index];
			List<Node> nodes = PointCloudTools.getNodesRecursively(root);
			int nodeIndex = 0;
			while (nodeIndex < nodes.size()) {
				if (PointCloudTools.isNodeSelected(nodes.get(nodeIndex))) {
					list.add(nodes.get(nodeIndex));
				}
				nodeIndex++;
			}
			index++;
		}
		return list;
	}
	/**
	 * Returns true if the given node is selected in the selection of the
	 * current workbench. Otherwise, false is returned.
	 *
	 * @param node The node that should be checked
	 * @return true If the node is selected and false otherwise
	 */
	public static boolean isNodeSelected(Node node) {
		Workbench workbench = Workbench.current();
		if (workbench == null) {
			return false;
		}
		Object object = UIProperty.WORKBENCH_SELECTION.getValue(workbench);
		if (!(object instanceof GraphSelection)) {
			return false;
		}
		GraphSelection selection = (GraphSelection)(object);
		return selection.contains(node.getGraph(), node, true);
	}
	/**
	 * Writes the given point clouds to the given file. If the point cloud list
	 * contains multiple point clouds, the point cloud index is added as fourth
	 * value in each line (point cloud id).
	 *
	 * @param file The file to write the point cloud(s) to
	 * @param pointClouds The point clouds to export
	 */
	public static void writePointCloudFile(String file, List<PointCloud> pointClouds) {
		String separator = ", ";
		List<String> lines = new ArrayList<String>();
		boolean singleMode = pointClouds.size() == 1;
		int cloud = 0;
		while (cloud < pointClouds.size()) {
			List<Point> points = pointClouds.get(cloud).getPoints();
			int point = 0;
			while (point < points.size()) {
				Point data = points.get(point);
				String line = data.getX() + separator + data.getY() + separator + data.getZ();
				if (!singleMode) {
					line += separator + cloud;
				}
				lines.add(line);
				point++;
			}
			cloud++;
		}
		PointCloudTools.writeFile(file, lines);
	}
	/**
	 * Writes the given list of strings as lines to the given file. If the file
	 * could not be written, an error message is shown in the terminal.
	 *
	 * @param file The file to write the lines to
	 * @param lines The strings to write as lines into the file
	 */
	public static void writeFile(String file, List<String> lines) {
		Path path = Paths.get(file);
		StringBuffer buffer = new StringBuffer();
		int index = 0;
		while (index < lines.size()) {
			buffer.append(lines.get(index));
			if (index < lines.size() - 1) {
				buffer.append("\n");
			}
			index++;
		}
		try {
			Files.write(path, buffer.toString().getBytes());
		} catch (IOException ioException) {
			String message = Resources.translate("fileCouldNotBeWritten");
			System.out.println(message + ": " + file);
			ioException.printStackTrace();
		}
	}
	/**
	 * Reads the given file and imports all contained points. The list of point
	 * clouds is returned. If the file could not be read, the returned list of
	 * points is empty. If the file contains multiple point clouds (identified
	 * by the optional point cloud ids), the list contains multiple elements.
	 * Otherwise, only one point cloud is contained. If the file contains
	 * invalid lines, an exception is thrown.
	 *
	 * @param file The name or path of the file that should be read
	 * @return The list of point clouds or an empty list if the file could not
	 * be read
	 * @throws IllegalArgumentException If one of the lines in the file is
	 * invalid
	 */
	public static List<PointCloud> readPointCloudFile(String file) {
		List<String> lines = PointCloudTools.readFile(file);
		if (lines == null) {
			return new ArrayList<PointCloud>();
		}
		List<Point> points = new ArrayList<Point>();
		List<Integer> ids = new ArrayList<Integer>();
		int maximumId = 0;
		// read raw data
		int index = 0;
		while (index < lines.size()) {
			Object[] data = PointCloudTools.parsePointLine(lines.get(index));
			if (data == null) {
				String message = Resources.translate("invalidLineInFileError");
				String error = message + ": " + file + ":" + (index + 1);
				throw new IllegalArgumentException(error);
			}
			double x = (Double)(data[0]);
			double y = (Double)(data[1]);
			double z = (Double)(data[2]);
			points.add(new Point(x, y, z));
			int id = (Integer)(data[3]);
			ids.add(id);
			if (id > maximumId) {
				maximumId = id;
			}
			index++;
		}
		PointCloud[] array = new PointCloud[maximumId + 1];
		index = 0;
		while (index < array.length) {
			array[index] = new PointCloud();
			index++;
		}
		index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			int id = ids.get(index);
			array[id].addPoint(point);
			index++;
		}
		List<PointCloud> clouds = new ArrayList<PointCloud>();
		index = 0;
		while (index < array.length) {
			clouds.add(array[index]);
			index++;
		}
		return clouds;
	}
	
	private static boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}

	
	/**
	 * Parses a text line of a point cloud file and returns the fitting point
	 * data. If the line does not contain three 'double' values, null is
	 * returned. If the line contains a fourth value of type 'int', the point
	 * gets a point cloud id. Otherwise, the id is set to 0. There returned
	 * object array consists of four elements: x, y, z and the id.
	 *
	 * @param line The line to parse the point from
	 * @return The parsed point data or null in case of an invalid line
	 */
	private static Object[] parsePointLine(String line) {
		line = line.replaceAll(",", " ");
		Scanner scanner = new Scanner(line);
		// 'locale' is important to get double variables with points
		// (not german numbers with commas)
		scanner.useLocale(Locale.US);
		Double x = null;
		Double y = null;
		Double z = null;
		Integer id = null;
		boolean valid = true;
		try {
			x = new Double(scanner.nextDouble());
			y = new Double(scanner.nextDouble());
			z = new Double(scanner.nextDouble());
			try {
				id = new Integer(scanner.nextInt());
			} catch (Exception exception) {
				id = new Integer(0);
			}
		} catch (Exception exception) {
			String message = Resources.translate("lineDoesNotContainThreeDoublesError");
			System.out.println(message + ": \"" + line + "\"");
			valid = false;
		}
		if (scanner != null) {
			scanner.close();
		}
		if (valid) {
			return new Object[]{x, y, z, id};
		} else {
			return null;
		}
	}
	/**
	 * Reads the given file and returns the list of text lines of the file. If
	 * the file could not be read, null is returned and an error message is
	 * shown in the terminal. The stack trace is also printed.
	 *
	 * @param file The file to read
	 * @return The list of lines of the file or null in case of an error
	 */
	public static List<String> readFile(String file) {
		try {
			return Files.readAllLines(Paths.get(file));
		} catch (IOException ioException) {
			String message = Resources.translate("fileCouldNotBeRead");
			System.out.println(message + ": " + file);
			ioException.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * clone selected pointclouds and add the clone to the project graph at root position
	 * 
	 * 
	 * @param item
	 * @param information
	 * @param context
	 * @throws CloneNotSupportedException
	 */
	
	public static void dublicate(Item item, Object information, Context context) throws CloneNotSupportedException {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		for(Object o: objects) {
			Node n =  ((Node)o).cloneGraph(EdgePatternImpl.FORWARD,false);
			IMP.addNode(item, n, context);
		}
	}
	
	public static void toCloudofPoints(Item item, Object information, Context context) {
		List<Object> objects = PointCloudTools.loadSelectedObjects(context);
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.secureCastPointClouds(objects);
	
		for(de.grogra.imp3d.objects.PointCloud p: array) {
			Node root= new Node();
			int len=p.getPoints().length;
			
			for(int i=0; i<len; i+=3){
				de.grogra.imp3d.objects.Point point = new de.grogra.imp3d.objects.Point(p.getPoints()[i],p.getPoints()[i+1],p.getPoints()[i+2]);
				root.addEdgeBitsTo(point, Graph.BRANCH_EDGE, null);	
			}
			IMP.addNode(item, root, context);
		}
		PointCloudTools.removeSelectedObjectsFrom3DView(context);
	}
	
	
	/**
	 * turning 3d models into pointcloud using the ply filter, by exporting the model to a temporary ply file, than turn that file to a temporary xyz file (using line by line processing) 
	 *  
	 * 
	 * @param item
	 * @param information
	 * @param context
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	
	
	public static void turn3DtoPointCloud(Item item, Object information, Context context) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {
			File tempFile_ply = File.createTempFile("prefix-", ".ply");
			File tempFile_xyz = File.createTempFile("prefix-", ".xyz");
			de.grogra.rgg.Library.export3DScene(tempFile_ply, "ply");
			 BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile_xyz));
			BufferedReader reader = new BufferedReader(new FileReader(tempFile_ply));
			String line = reader.readLine();
			boolean start=false;
			while (line != null) {
				if( line.equals("end_header")) {
					start=true;
					
				}
				if(start) {
					String[] lineParts = line.split(" ");
					if(lineParts.length>5) {
						
						writer.write(lineParts[0]+" , "+lineParts[1]+" , "+lineParts[2]+"\n");
					}
					
				}
				// read next line
				line = reader.readLine();
			}
		    reader.close();
		    writer.close();
		    Node root = context.getWorkbench().getRegistry().getProjectGraph().getRoot();
			root.removeAll(null);
		    
		    de.grogra.imp3d.objects.PointCloud[] array;
			array = PointCloudTools.importFromFile(tempFile_xyz.getAbsolutePath());
			PointCloudTools.addNodesToTree(array, context);
			return;
	}
}