package de.grogra.pointcloud;

import java.io.File;
import java.io.IOException;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;

public class PointCloudInput extends FilterBase implements ObjectSource {

	
	public PointCloudInput(FilterItem item, FilterSource source)  {
		
		super(item, source);
		setFlavor(IOFlavor.valueOf(Node.class));	
		
	}

	@Override
	public Object getObject() throws IOException {
		File f = ((FileSource) source).getInputFile ();	
		de.grogra.imp3d.objects.PointCloud[] array;
		array = PointCloudTools.importFromFile(f.getAbsolutePath());
		Node tmpRoot = new Node();
		for(de.grogra.imp3d.objects.PointCloud a : array) {
			tmpRoot.addEdgeBitsTo(a,Graph.BRANCH_EDGE, null);
		}
		return tmpRoot;
	}
}