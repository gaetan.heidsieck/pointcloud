/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a frustum. This class is used for the point cloud
 * fitting feature because it is much more simple to use than the original
 * class.
 *
 * @author Maurice Mueller
 */
public class Frustum extends Shape3D {
	/**
	 * The base radius of this frustum
	 */
	private double baseRadius;
	/**
	 * The top radius of this frustum
	 */
	private double topRadius;
	/**
	 * The length of this frustum
	 */
	private double length;
	/**
	 * Creates a frustum object with a position, a direction, a base radius, a
	 * top radius and a length.
	 *
	 * @param position The position for this frustum
	 * @param direction The direction for this frustum
	 * @param base The base radius for this frustum
	 * @param top The top radius for this frustum
	 * @param length The length for this frustum
	 */
	public Frustum(Vector position, Vector direction, double base, double top, double length) {
		super(position, direction);
		this.baseRadius = base;
		this.topRadius = top;
		this.length = length;
	}
	/**
	 * Sets the base radius of this frustum.
	 *
	 * @param radius The new base radius for this frustum
	 */
	public void setBaseRadius(double radius) {
		this.baseRadius = radius;
	}
	/**
	 * Returns the base radius of this frustum.
	 *
	 * @return The base radius of this frustum
	 */
	public double getBaseRadius() {
		return this.baseRadius;
	}
	/**
	 * Sets the top radius of this frustum.
	 *
	 * @param radius The new top radius for this frustum
	 */
	public void setTopRadius(double radius) {
		this.topRadius = radius;
	}
	/**
	 * Returns the top radius of this frustum.
	 *
	 * @return The top radius of this frustum
	 */
	public double getTopRadius() {
		return this.topRadius;
	}
	/**
	 * Sets the length of this frustum.
	 *
	 * @param length The new length for this frustum
	 */
	public void setLength(double length) {
		this.length = length;
	}
	/**
	 * Returns the length of this frustum.
	 *
	 * @return The length of this frustum
	 */
	public double getLength() {
		return this.length;
	}
	/**
	 * Calculates and returns the minimum distance of the given point to the
	 * surface of this frustum.
	 *
	 * WARNING: If the point is above or below the frustum, only the distance to
	 * the top or bottom surface is returned. The distance may be wrong in that
	 * case!
	 *
	 * @param point The point to get the minimum distance to the surface of this
	 * frustum for
	 * @return The minimum distance of the given point to the surface of this
	 * frustum
	 */
	public double getMinimumDistanceToSurface(Point point) {
		// The distance must be calculated for the base, for the top and for the
		// circular surface individually. Then, the minimum distance is returned
		Vector normal = this.getDirection().clone();
		normal.trimToLength(this.length);
		Vector bottomPosition = this.getPosition();
		Vector topPosition = Vector.addVectors(bottomPosition, normal);
		double distanceBase = point.getSignedDistanceToPlane(bottomPosition, normal);
		double distanceTop = point.getSignedDistanceToPlane(topPosition, normal);
		// If the distance to the base plane is negative, the point is below the
		// cylinder. The distance to the cylindrical surface is then ignored and
		// the positive distance to the base plane is returned.
		if (distanceBase < 0) {
			return -distanceBase;
		}
		// If the distance to the top plane is positive, the point is above the
		// cylinder. The distance to the cylindrical surface is then ignored and
		// the distance to the top plane is returned.
		if (distanceTop > 0) {
			return distanceTop;
		}
		// Here, the distanceBase is between 0 and length. The distance to the
		// relational radius of the frustum can be calculated now. The minimum
		// of the distance to the cylindrical surface, the distance to the base
		// surface and the distance to the top surface is then returned.
		// This is magic: The radius is calculated with the partial base radius
		// and the partial top radius.
		// Example: baseRadius = 10, topRadius = 8, ratio = 0.75
		// then, the radius is 10 * (1.0 - 0.75) + 8 * 0.75 = 2.5 + 6.0 = 8.5
		double ratio = distanceBase / this.length;
		double heightDependentRadius = this.baseRadius * (1.0 - ratio) + this.topRadius * ratio;
		double distanceSide = point.getDistanceToLine(bottomPosition, normal);
		distanceSide = Math.abs(heightDependentRadius - distanceSide);
		// distanceTop is less than 0 because the point is below the top surface
		if (distanceBase < distanceSide && distanceBase < -distanceTop) {
			return distanceBase;
		} else if (distanceSide < distanceBase && distanceSide < -distanceTop) {
			return distanceSide;
		} else {
			return distanceTop;
		}
	}
	/**
	 * Calculates and returns the volume of this frustum.
	 *
	 * @return The volume of this frustum
	 */
	public double getVolume() {
		double baseBase = this.baseRadius * this.baseRadius;
		double baseTop = this.baseRadius * this.topRadius;
		double topTop = this.topRadius * this.topRadius;
		return Math.PI * this.length / 3.0 * (baseBase + baseTop + topTop);
	}
	/**
	 * Calculates and returns the surface of this frustum.
	 *
	 * @return The surface of this frustum
	 */
	public double getSurface() {
		double difference = this.baseRadius - this.topRadius;
		double diagonal = Math.sqrt(this.length * this.length + difference * difference);
		double top = this.topRadius * this.topRadius * Math.PI;
		double side = (this.topRadius + this.baseRadius) * Math.PI * diagonal;
		double base = this.baseRadius * this.baseRadius * Math.PI;
		return base + side + top;
	}
}