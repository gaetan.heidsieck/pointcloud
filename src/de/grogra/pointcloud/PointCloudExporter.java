package de.grogra.pointcloud;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.grogra.util.Map;
import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.UserFields;

public class PointCloudExporter extends de.grogra.pf.io.FilterBase implements FileWriterSource {
	

	int id=0;	
		public PointCloudExporter(FilterItem item, FilterSource source) {
			super(item, source);
			setFlavor (item.getOutputFlavor ());


		}

		public void write(File out) throws IOException {
			List<Node> nodeList= PointCloudTools.getNodesRecursively((Node)Workbench.current().getRegistry().getProjectGraph().getRoot());
			List<Object> objects = new ArrayList<Object>();;//PointCloudTools.loadSelectedObjects(Workbench.current());
			
			for(Node n : nodeList) {
				System.out.println(n);
				if(n instanceof de.grogra.imp3d.objects.PointCloud) {
					objects.add(n);
				}
				
			}			
			if (!PointCloudTools.checkPointCloudSelection(objects, 1)) {
				return;
			}
			de.grogra.imp3d.objects.PointCloud[] array;
			array = PointCloudTools.secureCastPointClouds(objects);
			PointCloudDialog dialog = PointCloudDialog.getInstance();
			PointCloudTools.exportToFile(array, out.getAbsolutePath());
		
		}
		
}
